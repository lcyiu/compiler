import java.util.Vector;





abstract class Type
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	Type (String strName, int size)
	{
		setName(strName);
		setSize(size);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String
	getName ()
	{
		return m_typeName;
	}

	private void
	setName (String str)
	{
		m_typeName = str;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int
	getSize ()
	{
		return m_size;
	}

	protected void
	setSize (int size)
	{
		m_size = size;
	}

	public boolean
	isSameType(Type t){

		if(this.isInt()){
			if(t.isInt()){
				return true;
			}
			else{
				return false;
			}
		}
		if(this.isFloat()){
			if(t.isFloat()){
			
				return true;
			}
			else{
				return false;
			}
		}
		if(this.isBool()){
			if(t.isBool()){
				return true;
			}
			else{
				return false;
			}
		}
		if(this.isArray()){
			if(t.isArray()){
				return this.isEquivalent(t);
			}
			else{
				return false;
			}
		}
		if(this.isPointer()){
			if(t.isPointer()){
				return this.isEquivalent(t);
			}
			else{
				return false;
			}
		}
		if(this.isStruct()){
			if(t.isStruct()){
				if (((StructType)this).getactualName().equals(((StructType)t).getactualName()))
					return true;
				else
					return false;
			}
			else{
				return false;
			}
		}
		if(this.isFunctionPointer()){
			if(t.isFunctionPointer()){
				return this.isEquivalent(t);
			}
			else{
				return false;
			}
		}
		return false;
	}
	

	//----------------------------------------------------------------
	//	It will be helpful to ask a Type what specific Type it is.
	//	The Java operator instanceof will do this, but you may
	//	also want to implement methods like isNumeric(), isInt(),
	//	etc. Below is an example of isInt(). Feel free to
	//	change this around.
	//----------------------------------------------------------------
	public boolean	isInt ()	{ return false; }
	public boolean	isBool()	{ return false; }
	public boolean 	isBasic()	{ return false; }
	public boolean 	isNumeric()	{ return false; }
	public boolean 	isFloat()	{ return false; }
	public boolean 	isVoid()	{ return false; }
	public boolean 	isCompsite()	{ return false; }
	public boolean 	isArray()	{ return false; }
	public boolean 	isStruct()	{ return false; }
	public boolean 	isPointerGroup()	{ return false; }
	public boolean 	isPointer()	{ return false; }
	public boolean 	isFunctionPointer()	{ return false; }
	public boolean 	isNullPointer()	{ return false; }
	
	
	public boolean	isAssignable (Type t) { return false; }
	public boolean	isEquivalent (Type t) { return false; }
	
	public boolean  getIsFuncPtr() {return isfuncptr;}
	public void setIsFuncPtr(boolean b) { isfuncptr = b;}
	


	//----------------------------------------------------------------
	//	Name of the Type (e.g., int, bool, or some typedef
	//----------------------------------------------------------------
	private boolean 	isfuncptr = false;
	private String  	m_typeName;
	private int		m_size;
	public void setParamName(String string) {
		// TODO Auto-generated method stub
		setName(string);
	}


	public void setSizeNParam(int s_size, Vector<STO> members) {
		// TODO Auto-generated method stub
		setSize(s_size);
		
	}
}
