
public class NotOp extends UnaryOp{
	public NotOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isNot()	{ return true; }
	
	public STO checkOperand(STO a)
	{
		Type aType = a.getType();

		if (aType.isBool() )
		{
			if (a.isConst())
			{
				String aConst = (((ConstSTO)a).getBoolValue()) ? "false":"true";

				return new ConstSTO(aConst, new BoolType("bool",4));
			}
			else
				return new ExprSTO(this.getOperatorName() + a.getName(), new BoolType("bool",4));
		}
		else 
		{
			String ErrorStr = Formatter.toString(ErrorMsg.error1u_Expr, aType.getName(), this.getOperatorName(), "bool");
			return new ErrorSTO(ErrorStr);
		}
	}
}
