
public class IntType extends NumericType{

	public IntType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isInt()		{ return true;}
	
	public boolean isAssignable (Type t)
	{
		return t.isInt();
	}
	
	public boolean isEquivalent (Type t)
	{
		return t.isInt();
	}

}
