public class BitwiseOp extends BinaryOp{
	public BitwiseOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isBitwise ()		{ return true; }
	
	public STO checkOperands(STO a, STO b) { 
		
		Type aType = a.getType();
		Type bType = b.getType();

		if(aType.isInt() && bType.isInt()){
			if (a.isConst() && b.isConst())
			{
				int aConst = ((ConstSTO)a).getIntValue();
				int bConst = ((ConstSTO)b).getIntValue();
				int resConst;
				
				if (this.isBwAnd())
				{		
					resConst = aConst & bConst;
				}
				else if (this.isBwOr())
				{
					resConst = aConst | bConst;
				}
				else
				{
					resConst = aConst ^ bConst;
				}
				
				ConstSTO res = new ConstSTO(""+resConst, new IntType("int", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new IntType("int",4));
			
		}
		else if (!aType.isInt())
		{
					String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, aType.getName(), this.getOperatorName(), "int");
					return new ErrorSTO(ErrorStr);
		}
		else
		{
			String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, bType.getName(), this.getOperatorName(), "int");
			return new ErrorSTO(ErrorStr);
		}
		
	}
}


