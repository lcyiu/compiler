public class ComparisonOp extends BinaryOp{
	public ComparisonOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isComparison ()		{ return true; }
	
	public STO checkOperands(STO a, STO b)
	{
		Type aType = a.getType();
		Type bType = b.getType();
		if(!(aType.isNumeric()) || !(bType.isNumeric())){
			if(!(aType.isNumeric())){
				String ErrorStr = Formatter.toString(ErrorMsg.error1n_Expr, aType.getName(), this.getOperatorName());
				return new ErrorSTO(ErrorStr);
			}
			else{
				String ErrorStr = Formatter.toString(ErrorMsg.error1n_Expr, bType.getName(), this.getOperatorName());
				return new ErrorSTO(ErrorStr);
			}
		}
		else 
		{
			if (a.isConst() && b.isConst())
			{
				double aConst = ((ConstSTO)a).getValue();
				double bConst = ((ConstSTO)b).getValue();
				String resConst;
				
				if (this.isGreaterThan())
				{
					resConst = (aConst > bConst)? "true":"false";
				}
				else if (this.isGreaterThanEqual())
				{
					resConst = (aConst >= bConst)? "true":"false";
				}
				else if (this.isLessThan())
				{
					resConst = (aConst < bConst)? "true":"false";
				}
				else if (this.isLessThanEqual())
				{
					resConst = (aConst <= bConst)? "true":"false";
				}
				else if (this.isEqual())
				{
					resConst = (aConst == bConst)? "true":"false";
				}
				else 
				{
					resConst = (aConst != bConst)? "true":"false";
				}
				
				ConstSTO res = new ConstSTO(resConst, new BoolType("bool", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new BoolType("bool",4));
		}
	}
}

	