
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class VarSTO extends STO
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	VarSTO (String strName)
	{
		super (strName);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	}

	public 
	VarSTO (String strName, Type typ)
	{
		super (strName, typ);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	
		if (!typ.isArray())
			setIsModLValue(true);
		else
		{
			setIsAddressable(true);
		}
	}
	
	public 
	VarSTO (String strName, Type typ, boolean t)
	{
		super (strName, typ);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	
		if (t)
			setIsModLValue(true);
		else
		{
			setIsAddressable(true);
		}
		
	}
	
	public 
	VarSTO (String strName, Type typ, boolean t, STO n, STO barr)
	{
		super (strName, typ);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	
		if (t)
			setIsModLValue(true);
		else
		{
			setIsAddressable(true);
		}
		
		setIsArray(true);
		setnIndex(n);
		setbArray(barr);
		
	}
	
	public 
	VarSTO (String strName, Type typ, boolean t, boolean isGlobal)
	{
		super (strName, typ);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	
		if (t)
			setIsModLValue(true);
		else
		{
			setIsAddressable(true);
		}
		
		setIsGlobal (isGlobal);
	}
	
	public 
	VarSTO (String strName, Type typ, String addressOf)
	{
		super (strName, typ);
		// You may want to change the isModifiable and isAddressable 
		// fields as necessary
	
		if (addressOf.equals("true"))
			setIsModLValue(false);
		else
		{
			setIsModLValue(true);
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean   
	isVar () 
	{
		return true;
	}
}
