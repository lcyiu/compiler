public class EqualOp extends ComparisonOp{
	public EqualOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isEqual()		{ return true; }
	
	public STO checkOperands(STO a, STO b)
	{
		Type aType = a.getType();
		Type bType = b.getType();
		if (aType.isNumeric() && bType.isNumeric() || aType.isBool() && bType.isBool())
		{
			if (a.isConst() && b.isConst())
			{

				String resConst = (((ConstSTO)a).getValue() - ((ConstSTO)b).getValue() == 0)?"true":"false";
			
				ConstSTO res = new ConstSTO(resConst, new BoolType("bool", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new BoolType("bool",4));
		}
		else 
		{
			if (aType.isPointerGroup() && bType.isPointerGroup())
			{
				if (aType.isNullPointer() && aType.isNullPointer())
				{
					return new ConstSTO ("true", new BoolType("bool",4));
				}
				else
				{
					if (aType.isNullPointer() || bType.isNullPointer())
					{
						return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new BoolType("bool",4));
					}
					else
					{
						if (aType.isEquivalent(bType))
							return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new BoolType("bool",4));
						else
						{
							String ErrorStr = Formatter.toString(ErrorMsg.error17_Expr, this.getOperatorName(),aType.getName(), bType.getName());
							return new ErrorSTO(ErrorStr);
						}
					}
				}
			}
			else
			{
				if (aType.isPointerGroup() || bType.isPointerGroup())
				{
					String ErrorStr = Formatter.toString(ErrorMsg.error17_Expr, this.getOperatorName(),aType.getName(), bType.getName());
					return new ErrorSTO(ErrorStr);
				}
				else
				{
					String ErrorStr = Formatter.toString(ErrorMsg.error1b_Expr, aType.getName(), this.getOperatorName(), bType.getName());
					return new ErrorSTO(ErrorStr);
				}
			}
		}
	}
}

