
public class PointerType extends PointerGroupType{
	public PointerType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public PointerType(String strName, int size , int n, Type t) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setStar ( n);
		setpointType(t);
		setAddressOf(false);
	}
	
	public PointerType(String strName, int size , int n, Type t, boolean b) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setStar ( n);
		setpointType(t);
		setAddressOf(b);
	}
	
	
	
	private void setStar (int x)
	{
		numOfStar = x;
	}
	public int getStar() { return numOfStar; }

	public boolean isPointer() 	{return true;}
	
	public Type getpointType() { return pointType; }

	private void setpointType (Type x)
	{
		pointType = x;
	}
	
	private void setAddressOf (boolean t)
	{
		addressOf = t;
	}
	public boolean getAddressOf (){ return addressOf; }
	
	private int numOfStar = 0;
	private Type pointType = null;
	private boolean addressOf = false;
	
	public boolean	isAssignable (Type t) 
	{ 
	//	System.out.println("CHECK");
		if(t.isArray()){
			ArrayType ta = (ArrayType)t;
			
			int star = this.getStar();
			
			
			
			//int counter1 = 1;
			//while(ta.getBaseType().isArray()){
			//	ta = (ArrayType)ta.getBaseType();
			//	counter1++;
			//}
	//		System.out.println("counter: " + counter1);
	//		System.out.println("star: " + this.getStar());
			//if(this.getStar() == counter1){
			//	System.out.println("star: " + this.getpointType().getName() + "this: " + ta.getBaseType().getName());
			if (star == 1)	
				return this.getpointType().isSameType(ta.getBaseType());
			else
			{
				PointerType pt = new PointerType(t.getName(), this.getSize(), this.getStar()-1,this.getpointType(),this.getAddressOf());
				return pt.isSameType(ta.getBaseType());
			}
			//}
			//return false;
		}
		
		return isEquivalent(t);
		
	
	}
	public boolean	isEquivalent (Type ta)
	{
		int first = 0;
		int second = 0;
		if (ta.isNullPointer())
		{
			return true;
		}
		
		if(ta.isPointer()){
			PointerType t = (PointerType)ta;
			PointerType tb = (PointerType)this;
			while(t.getpointType().isPointer()){
				first+=t.getStar();
				t = (PointerType)t.getpointType();
				
			}
			while(tb.getpointType().isPointer()){
				second+=tb.getStar();
				tb = (PointerType)tb.getpointType();
				
			}
		//	System.out.println("first"  + first + "this: " + second);
		//	System.out.println("first"  + t.getStar()+first + "this: " + this.getStar()+second);
			if(t.getpointType().isSameType(tb.getpointType())){
				if(t.getStar()+first != tb.getStar()+second)
				{
					return false;
				}
				else{
					return true;
				}
			}
			else{
				return false;
			}
			
			
		}
		else{
			return false;
		}
	
		
	}

	
}
