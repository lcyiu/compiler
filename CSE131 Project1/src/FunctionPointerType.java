import java.util.Vector;


public class FunctionPointerType extends PointerGroupType {
	public FunctionPointerType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isFunctionPointer() 	{return true;}
	
	public FunctionPointerType(String strName, int size, Vector<STO> p, Type t, boolean b) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setParams(p);
		setReturnType(t);
		setIsRef(b);
	}
	
	public boolean isEquivalent(Type t)
	{
		return isAssignable(t);
	}
	
	public boolean isAssignable(Type t)
	{
		if (t.isNullPointer())
			return true;
		
		if (!t.isFunctionPointer())
		{
			return false;
		}
		else
		{
			FunctionPointerType fptrT= (FunctionPointerType)t;
			if (fptrT.getIsRef() != this.getIsRef())
				return false;
			else if (!fptrT.getReturnType().isEquivalent(this.getReturnType()) && 
					!(fptrT.getReturnType().isVoid() && this.getReturnType().isVoid()))
				return false;
			else
			{
				//Check param type
				Vector<STO> thisPs= this.getParams();
				Vector<STO> argPs = fptrT.getParams();
				
				if (argPs== null  && thisPs == null || argPs.size() ==0  && thisPs == null)
					return true;
				if (argPs ==null && thisPs != null || argPs !=null && thisPs == null ||
						argPs.size() ==0 && thisPs != null || argPs.size() !=0 && thisPs == null)
					return false;
				else if (thisPs.size() != argPs.size())
					return false;
				else
				{
					for (int i = 0; i < thisPs.size(); i++)
					{
						STO thisP = thisPs.get(i);
						STO argP = argPs.get(i);
						if (thisP.getIsRef() != argP.getIsRef())
							return false;
						else if (!thisP.getType().isEquivalent(argP.getType()))
						{
							return false;
						}
					}
				}
			}
			return true;
		}
		
		
	}
	
	public Vector<STO> getParams() { return params; }
	public Type getReturnType() { return returnType; }
	public boolean getIsRef() { return isRef; }
	
	private void setParams(Vector<STO> p) { params = p; }
	private void setReturnType(Type t) { returnType = t; }
	private void setIsRef(boolean b) { isRef = b; }
	
	
	private Vector<STO> params;
	private Type returnType;
	private boolean isRef = false;
	
}
