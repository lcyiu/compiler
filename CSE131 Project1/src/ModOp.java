public class ModOp extends ArithmeticOp{
	public ModOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isMod()		{ return true; }
	
	public STO checkOperands(STO a, STO b) { 
		
		Type aType = a.getType();
		Type bType = b.getType();

		if(aType.isInt() && bType.isInt()){
			if (a.isConst() && b.isConst())
			{
				int aConst = ((ConstSTO)a).getIntValue();
				int bConst = ((ConstSTO)b).getIntValue();
				int resConst;
				
				if (bConst == 0)
				{	
					String ErrorStr = ErrorMsg.error8_Arithmetic;
					return new ErrorSTO(ErrorStr);
				}
				else
					resConst = aConst % bConst;
				
				
				ConstSTO res = new ConstSTO(""+ resConst, new IntType("int", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new IntType("int",4));
		}
		else if (!aType.isInt())
		{
					String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, aType.getName(), this.getOperatorName(), "int");
					return new ErrorSTO(ErrorStr);
		}
		else
		{
			String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, bType.getName(), this.getOperatorName(), "int");
			return new ErrorSTO(ErrorStr);
		}
		
	}
}

