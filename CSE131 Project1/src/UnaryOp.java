public class UnaryOp extends Operator{
	public UnaryOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isUnary ()		{ return true; }
	
	public STO checkOperand(STO a)
	{
		Type aType = a.getType();
		
		if ((!aType.isNumeric() && !aType.isPointer() )|| aType.isNullPointer())
		{
			String ErrorStr = Formatter.toString(ErrorMsg.error2_Type, aType.getName(), this.getOperatorName());
			return new ErrorSTO(ErrorStr);
		}
		else if (!a.isModLValue())
		{
			String ErrorStr = Formatter.toString(ErrorMsg.error2_Lval, this.getOperatorName());
			return new ErrorSTO(ErrorStr);
		}
		else
		{
			if (aType.isFloat())
			{
				return new ExprSTO(this.getOperatorName() + a.getName(), a.getType());
			}
			else if (aType.isInt())
			{
				return new ExprSTO(this.getOperatorName() + a.getName(), a.getType());
			}
			else
			{
				return new ExprSTO(this.getOperatorName() + a.getName(), a.getType());
			}
		}
		
	}
}

