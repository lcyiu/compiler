
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;

public class AssemblyCodeGenerator {
    
	public boolean isAndOr = false;
    private int indent_level = 0;
    
    private int str_num = 0;
    
    private int bool_num = 0;
    private int and_num  = 0;
    private int or_num = 0;
    private int endif_num = 0;
    private int var_num = 0;
    private int while_num = 0;
    private int const_and_num = 0;
    private int const_or_num = 0;
    private int deref_num = 0;
    private int array_bound = 0;
    
    private static final String ERROR_IO_CLOSE = 
        "Unable to close fileWriter";
    private static final String ERROR_IO_CONSTRUCT = 
        "Unable to construct FileWriter for file %s";
    private static final String ERROR_IO_WRITE = 
        "Unable to write to fileWriter";

    private FileWriter fileWriter;
    
    private static final String FILE_HEADER = 
        "/*\n" +
        " * Generated %s\n" + 
        " */\n\n";
    
    //------------------------------------------------------------------------------------------
    //
    //	Scoping and function name
    //
    //------------------------------------------------------------------------------------------
    private int offset = 0;
    
    private int getNextOffset() { offset -= 4; return offset; }
    private int getOffset() { return offset; }
    private void setOffset(int i) { offset = i; }
    //------------------------------------------------------------------------------------------
    //
    //	Scoping and function name
    //
    //------------------------------------------------------------------------------------------
    private String scope = "global";
    private String funcName ="";
    
    private String getScope() { return scope; }
    private void setScope(String s) { scope = s; }
    
    private String getFuncName() { return funcName; }
    private void setFuncName(String s) { funcName = s; }
    
    //------------------------------------------------------------------------------------------
    //
    //	Stack that store variables
    //
    //------------------------------------------------------------------------------------------
    private Vector<STO> staticglobalStack ;
    private Vector<STO> literalStack ;
    private Vector<STO> localStack ;
    private Vector<STO> localStaticStack;
    private Vector<STO> localStaticValueStack;
    private Vector<Integer> ifStack;
    private Vector<Integer> whileStack;
    private Vector<Integer> AndOrStack;
    
    private void addStaticGlobal(STO sto) { staticglobalStack.addElement(sto); }
    private Vector<STO> getStaticGlobal() { return staticglobalStack; }
    
    private void addLiteral(STO sto) { literalStack.addElement(sto); }
    private Vector<STO> getLiteral() { return  literalStack; }
    
    private void addLocal(STO sto) { localStack.addElement(sto); }
    private Vector<STO> getLocal() { return  localStack; }
    
    private void addLocalStatic(STO sto) { localStaticStack.addElement(sto); }
    private Vector<STO> getLocalStatic() { return  localStaticStack; }
    
    private void addLocalStaticValue(STO sto) { localStaticValueStack.addElement(sto); }
    private Vector<STO> getLocalStaticValue() { return  localStaticValueStack; }
    
    private void addIfNum(int i) { ifStack.addElement(i); }
    private Vector<Integer> getIfStack() { return  ifStack; }
    
    private void addWhileNum(int i) { whileStack.addElement(i); }
    private Vector<Integer> getWhileStack() { return  whileStack; }
    
    private void addAndOrNum(int i) { AndOrStack.addElement(i); }
    private Vector<Integer> getAndOrStack() { return  AndOrStack; }
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////									 	  //////////////////////////////////
    ///////////////////		Beginning of all the Functions    //////////////////////////////////
    ///////////////////									      //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    //------------------------------------------------------------------------------------------
    //
    //	Constructor
    //
    //------------------------------------------------------------------------------------------
    public AssemblyCodeGenerator(String fileToWrite) {
        try {
            fileWriter = new FileWriter(fileToWrite);
            
            // 7
            writeAssembly(FILE_HEADER, (new Date()).toString());
        } catch (IOException e) {
            System.err.printf(ERROR_IO_CONSTRUCT, fileToWrite);
            e.printStackTrace();
            System.exit(1);
        }
        
        staticglobalStack= new Vector<STO>();
        literalStack= new Vector<STO>();
        localStack= new Vector<STO>();
        localStaticStack = new Vector<STO>();
        localStaticValueStack = new Vector<STO>();
        ifStack = new Vector<Integer>();
        whileStack = new Vector<Integer>();
        AndOrStack = new Vector<Integer>();
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Program Start, add all necessary header
    //
    //------------------------------------------------------------------------------------------
	public void DoProgramStart(String filename) {
        increaseIndent();
        
        writeCommentHeader("Starting Program");

        writeAssembly(SparcInstr.BLANK_LINE);

        DoROPrintDefines();
        //MakeGlobalInitGuard();

		
	}

    //------------------------------------------------------------------------------------------
    //
    //	Indentation
    //
    //------------------------------------------------------------------------------------------
    public void decreaseIndent() {
        indent_level--;
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Close File
    //
    //------------------------------------------------------------------------------------------
    public void dispose() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.err.println(ERROR_IO_CLOSE);
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Indentation
    //
    //------------------------------------------------------------------------------------------
    public void increaseIndent() {
        indent_level++;
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Write to File
    //
    //------------------------------------------------------------------------------------------
    public void writeAssembly(String template, String ... params) {
        StringBuilder asStmt = new StringBuilder();
        
        // 10
        for (int i=0; i < indent_level; i++) {
            asStmt.append(SparcInstr.INDENTOR);
        }
        
        // 11
        asStmt.append(String.format(template, (Object[])params));
        
        try {
            fileWriter.write(asStmt.toString());
        } catch (IOException e) {
            System.err.println(ERROR_IO_WRITE);
            e.printStackTrace();
        }
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Read Only Data
    //
    //------------------------------------------------------------------------------------------
    public void DoROPrintDefines()
    {
        // !----Default String Formatters----
        writeCommentHeader("Default String Formatters");

        // .section ".rodata"
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.SECTION_DIR, SparcInstr.RODATA_SEC);

        // _endl: .asciz "\n"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.ENDL, SparcInstr.ASCIZ_DIR, quoted("\\n"));

        // _intFmt: .asciz "%d"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.INTFMT, SparcInstr.ASCIZ_DIR, quoted("%d"));

        // _boolFmt: .asciz "%s"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.BOOLFMT, SparcInstr.ASCIZ_DIR, quoted("%s"));

        // _boolT: .asciz "true"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.BOOLT, SparcInstr.ASCIZ_DIR, quoted("true"));

        // _boolF: .asciz "false"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.BOOLF, SparcInstr.ASCIZ_DIR, quoted("false"));

        // _strFmt: .asciz "%s"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.STRFMT, SparcInstr.ASCIZ_DIR, quoted("%s"));
        
        // nullpointer
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.STRNULLPOINTER, SparcInstr.ASCIZ_DIR, 
        		quoted("Attempt to dereference NULL pointer.\\n"));
        
        // nullpointer
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.STROFB, SparcInstr.ASCIZ_DIR, 
        		quoted("Index value of %d is outside legal range [0,%d).\\n" ));
        
        
        // .align 
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.ALIGN_DIR, 4 +"");
        // _FloatINCDEC_1: .single 0r "%s"
        writeAssembly(SparcInstr.RO_DEFINE, SparcInstr.FloatINCDEC, SparcInstr.SINGLE_DIR, "1.00");
        
        
        
        writeAssembly(SparcInstr.BLANK_LINE);
    }

    //------------------------------------------------------------------------------------------
    //
    //	Comment 
    //
    //------------------------------------------------------------------------------------------ 
    public void writeComment(String comment)
    {
        // ! Comment
    	decreaseIndent();
        writeAssembly(SparcInstr.LINE, SparcInstr.COMMENT + " " + comment);
        increaseIndent();
    }
    
    //------------------------------------------------------------------------------------------
    //
    //	Comment Header
    //
    //------------------------------------------------------------------------------------------
	private void writeCommentHeader(String comment) {
		// TODO Auto-generated method stub
		writeAssembly(SparcInstr.BLANK_LINE);
        writeComment("|-------------------------------------------------------------------------");
        writeComment("|      " + comment);
        writeComment("|-------------------------------------------------------------------------");
        writeAssembly(SparcInstr.BLANK_LINE);
		
	}

    //------------------------------------------------------------------------------------------
    //
    //	"quote"
    //
    //------------------------------------------------------------------------------------------
	private String quoted(String string) {
		// TODO Auto-generated method stub
		return "\"" + string + "\"";
	}
		
    //------------------------------------------------------------------------------------------
    //
    //	Global Variable Declaration
    //
    //------------------------------------------------------------------------------------------
    public void DoGlobalDecl(STO varSto, STO valueSTO, boolean isStatic)
    {
    	//if declare as static in function scope, it will have different name
    	String finalName = varSto.getName();
    	
    	varSto.setIsStatic(isStatic);
    	   	
    	//If variable is local static, 
    	//save it and declare it after return of the function
    	if (isStatic && !getScope().equals("global"))
    	{
    		finalName = "yiu_"+getFuncName() + "_" + finalName +"_"+ var_num;
    		var_num++;
    		    		
    		writeComment("Declare Local Static: " + varSto.getName() + " at the end of function");
        	varSto.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
                              
            if (!CheckExistStaticGlobal(finalName))
            {
	            //if it does not exist in the stack yet
	            //Add Global/Static Variable to Stack 
            	
            	
	            addStaticGlobal(varSto);
	            addLocalStatic(varSto);
	            addLocalStaticValue(valueSTO);
            }
            /*
            //set value to the static variable
            if (!valueSTO.isEmpty())
            {
            	loadLiteralInReg(valueSTO, SparcInstr.REG_LOCAL1);
            	writeComment("Store " + valueSTO.getName() + " into " + varSto.getName());
				storeRegValueInMem(varSto, SparcInstr.REG_LOCAL1);
            }
            */
            writeAssembly(SparcInstr.BLANK_LINE);
            
            return;
    	}
    	    	
        writeComment("Declare Global/Static: " + varSto.getName());
        
        if (!varSto.getType().isArray() && ! varSto.getType().isStruct() && !varSto.getType().isPointer())
        {
	        // .section data
	        // .align 4
	        doSwitchSection(SparcInstr.DATA_SEC, "4");
        }
        else
        {
	        // .section bss
	        // .align 4
	        doSwitchSection(SparcInstr.BSS_SEC, "4");
        }
        
		if(!isStatic)
		{
			// .global <id>
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.GLOBAL_DIR, varSto.getName());
		}
		
		//id: .word x
		//id: .single 0rx
		writeVarInit(varSto, valueSTO, finalName);
		
		
        // set the base, offset, assembly name to the sto
        varSto.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
        writeAssembly(SparcInstr.BLANK_LINE);
        
        //Add Global/Static Variable to Stack 
        addStaticGlobal(varSto);

    }

    //------------------------------------------------------------------------------------------
    //
    //	Local Variable Declaration
    //
    //------------------------------------------------------------------------------------------
	public void DoVarDecl(STO varSto, STO valueSTO, boolean isConst) {
		
		//there is no initialization, no need to add code for it
		//only give address for it
		int offset;

		
		if (varSto.getType().isArray() || varSto.getType().isStruct() )
		{
			
			setOffset(getOffset() - varSto.getType().getSize());
			offset = getOffset();
		}
		else
		{
			offset= getNextOffset();
		}
			
		// set the base, offset, assembly name to the sto
		// assembly name will be nothing since it is stored in stack
		varSto.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		
		writeComment("Declare Local: " + varSto.getName());
		writeComment("base: "+varSto.getBase() + ", offset: "+varSto.getOffset());
		writeComment("Store " + valueSTO.getName() +" into "+ varSto.getName());
		
		if (!valueSTO.isEmpty())
		{
			DoAssign(varSto, valueSTO);
		}
		    
  
        writeAssembly(SparcInstr.BLANK_LINE);
		
      //Add Local Variable to Stack 
		addLocal(varSto);
	}
  
    //------------------------------------------------------------------------------------------
    //
    //	Function Start
    //
    //------------------------------------------------------------------------------------------
    public void DoFuncStart(FuncSTO funcSto) 
    {
    	setScope("function");
    	setFuncName(funcSto.getName());
    	
    	writeCommentHeader("Function: " + funcSto.getName());
    	
		increaseIndent();
		writeAssembly(SparcInstr.BLANK_LINE);
		
		// .text
		// .align 4
		doSwitchSection(SparcInstr.TEXT_SEC, "4");
        
        // .global <funcName>
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.GLOBAL_DIR, funcSto.getName());
        writeAssembly(SparcInstr.BLANK_LINE);
		
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, funcSto.getName());
		increaseIndent();
		
        // Move the saved offset for this function into %g1 and then execute the save instruction that shifts the stack
        // set SAVE.<funcName>, %g1
        writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.SAVE_WORD + "." + funcSto.getName(), SparcInstr.REG_GLOBAL1);
		
        // save %sp, %g1, %sp
        writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.SAVE_OP, SparcInstr.REG_STACK, SparcInstr.REG_GLOBAL1, SparcInstr.REG_STACK);
        writeAssembly(SparcInstr.BLANK_LINE);
        
    }
    
    public void writeParamInStack(FuncSTO sto) {
		// TODO Auto-generated method stub
    	
    	writeComment("Store %i0 - %i5 or %f0 - %f5 to stack space");
    	
    	String base = "%fp";
    	int offset = 68;
    	int f = 0;
    	int i = 0;
    	
    	for (int j = 0; j<sto.getNumParam(); j ++)
    	{
    		STO param = sto.getParam().get(j);
    		if (!param.getType().isFloat() || param.getIsRef())
    		{
    			writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.STORE_OP, "%i"+i++, "["+base+ " + "+ offset+"]", param.getName());
    		}
    		else
    		{
    			writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.STORE_OP, "%f"+f++, "["+base+ " + "+ offset+"]", param.getName());
    		}
    		
    		offset +=4;
    	}
    	writeAssembly(SparcInstr.BLANK_LINE);
		
	}
    
	//-------------------------------------------------------------------------
    //      DoFuncFinish
    //-------------------------------------------------------------------------
    public void DoFuncFinish(FuncSTO funcSto)
    {

        // Perform return/restore
        writeAssembly(SparcInstr.BLANK_LINE);

        // Also executed in DoReturn()
        if(funcSto.getReturnType().isVoid()) {
        	writeReturn();
        }

        // Write the assembler directive to save the amount of bytes needed for the save operation
        decreaseIndent();

        // SAVE.<func> = -(92 + BytesOfLocalVarsAndTempStackSpaceNeeded) & -8
        writeAssembly(SparcInstr.SAVE_FUNC, funcSto.getName(), String.valueOf(92), String.valueOf(-getOffset()));
        writeAssembly(SparcInstr.BLANK_LINE);
        
        DoDeclareLocalStatic();
        
        //resets
        setOffset(0);
        localStaticStack.clear();
        localStaticValueStack.clear();
        localStack.clear();
    	setScope("global");
    	setFuncName("");

    }
	
	//-------------------------------------------------------------------------
    //      Delcare all local static variable after return
    //-------------------------------------------------------------------------
    private void DoDeclareLocalStatic() {
			
        if (getLocalStatic().size() > 0)
        {
        	writeComment("Declare Local Static variable in " + getFuncName());
        	

	        
	        for (int i = 0; i < getLocalStatic().size(); i++)
	        {
	        	STO varSto = getLocalStatic().get(i);
	        	STO valueSTO = getLocalStaticValue().get(i);
	        	
	        	if (!varSto.getIsString())
	        	{
	        		if (!varSto.getType().isArray() && !varSto.getType().isStruct() &&!varSto.getType().isPointer())
	        		{
		                // .section data
		                // .align 4
		    	        doSwitchSection(SparcInstr.DATA_SEC, "4");
	        		}
	        		else
	        		{
		                // .section bss
		                // .align 4
		    	        doSwitchSection(SparcInstr.BSS_SEC, "4");
	        		}
	        	}
	        	else
	        	{
	        		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.SECTION_DIR, SparcInstr.RODATA_SEC);
	        	}
	        	writeVarInit(varSto, valueSTO, varSto.getAsmName());
	        }
        }
		
	}
    
    //-------------------------------------------------------------------------
    //      set	x, %reg		x is int literal
    //-------------------------------------------------------------------------
    public void loadLiteralInReg(STO sto, String reg) 
    {
    	writeComment("Set " + sto.getName() + " into " + reg);
        writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getName(), reg);  
        writeAssembly(SparcInstr.BLANK_LINE);
    }
    
    //-------------------------------------------------------------------------
    //      set	x, %reg		x is format
    //------------------------------------------------------------------------- 
    public void loadFormatInReg(String s, String reg) 
    {
    	writeComment("Set " + s + " into " + reg);
        writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, s, reg);  
        writeAssembly(SparcInstr.BLANK_LINE);
    }
    
    //-------------------------------------------------------------------------
    //      load float or int to float and set it into the reg
    //-------------------------------------------------------------------------
    public void loadFloatORIntInReg(STO sto, String reg)
    {
    	Type stoTyp = sto.getType();
    	String finalName ="zachfloat" + "_";
    	if (sto.getName().charAt(0) == '-')
    		finalName+=  "n"+sto.getName().substring(1);
    	else
    		finalName+=  sto.getName();
    	
		if(stoTyp.isInt())
		{
			finalName += ".0";
			sto.setType(new FloatType(stoTyp.getName(), stoTyp.getSize()));
		}
		
		writeComment("Declare " + sto.getName() + " at the end of function");
                      
        if (!CheckExistStaticGlobal(finalName))
        {
            //if it does not exist in the stack yet
            //Add Global/Static Variable to Stack 
        	
        	
            addStaticGlobal(sto);
            addLocalStatic(sto);
            addLocalStaticValue(sto);
        }
        sto.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
        
        loadStackInReg(sto, reg);
    }
    
    //-------------------------------------------------------------------------
    //      Store to register, register should already have value
    //		Store from L1 to [L0]
    //-------------------------------------------------------------------------
    public void storeRegValueInMem(STO sto, String reg) 
    {
    	if (sto.getIsRef())
    	{
    		if (!sto.getIsArray() && !sto.getIsStruct() && !sto.getIsDeref())
    		{
	    		writeComment("store value of "+reg+ " into reference "+ sto.getName());
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP,  reg, "[" + SparcInstr.REG_LOCAL0 + "]");
    		}
    		else if (sto.getIsArray())
    		{
    			storeArrayIndexInReg(sto, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			storeStructInReg(sto, reg);
    		}
    		else if (sto.getIsDeref())
    		{
    			storePointerInReg(sto, reg);
    		}
    	}
    	else
    	{
    		if (!sto.getIsArray() && !sto.getIsStruct() && !sto.getIsDeref())
    		{
			    writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
			    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
			    writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP,  reg, "[" + SparcInstr.REG_LOCAL0 + "]");
    		}
    		else if (sto.getIsArray())
    		{
    			storeArrayIndexInReg(sto, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			storeStructInReg(sto, reg);
    		}
    		else if (sto.getIsDeref())
    		{
    			storePointerInReg(sto, reg);
    		}
    	}
    	
    	writeAssembly(SparcInstr.BLANK_LINE);
    }
    
    private void storePointerInReg(STO sto, String reg) {
    	writeComment("Store value of "+reg+" into *" + sto.getName());
    	
    	loadBaseOffset(sto, sto.getbPointer());

    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP, reg, "[" + SparcInstr.REG_LOCAL4 + "]");
	}
    
	public void storeArrayIndexInReg(STO sto, String reg) {
    	writeComment("Store value of "+reg+" into " + sto.getName() + "["+ sto.getnIndex().getName()+"]");
    	
    	loadBaseOffset(sto, sto.getbArray());

    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP, reg, "[" + SparcInstr.REG_LOCAL4 + "]");
		
	}
    
    public void storeStructInReg(STO sto, String reg) {
    	
    	writeComment("store " +reg+" into " + sto.getbStruct().getName()+"."+sto.getName());
    	
    	STO tSTO = sto.getbStruct();
    	loadBaseOffset(sto, tSTO);

    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP, reg, "[" + SparcInstr.REG_LOCAL4 + "]");
		
	}
    
    //-------------------------------------------------------------------------
    //      Load to register, register value will be overwrite
    //		From [L0] loads to L1
    //-------------------------------------------------------------------------
	public void loadStackInReg(STO sto, String reg) 
    { 	
    	if (sto.getIsRef())
    	{
    		if (!sto.getIsArray() && !sto.getIsStruct())
    		{
	    		writeComment("Load value of reference " + sto.getName() + " into " + reg);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    	if(!sto.getIsDeref())
		    	{
		    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
		    	}
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", reg);
    		}
    		else if (sto.getIsArray())
    		{
    			loadArrayIndexInReg(sto, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			loadStructInReg(sto, reg);
    		}
    	}
    	else
    	{
    		if (!sto.getIsArray() && !sto.getIsStruct())
    		{
		    	writeComment("Load " + sto.getName() + " into " + reg);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", reg);
    		}
    		else if (sto.getIsArray())
    		{
    			loadArrayIndexInReg(sto, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			loadStructInReg(sto, reg);
    		}
    	}
    	writeAssembly(SparcInstr.BLANK_LINE);
    }
    
    public void loadStructInReg(STO sto, String reg) {
    	
    	writeComment("Load " + sto.getbStruct().getName()+"."+sto.getName()+" into " + reg);
    	
    	STO tSTO = sto.getbStruct();
    	
    	loadBaseOffset(sto, tSTO);
    	
    	
    	

		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL4 + "]", reg);

		
	}
    
	public void loadArrayIndexInReg(STO sto, String reg) {
		
    	writeComment("Load " + sto.getName() + "["+ sto.getnIndex().getName()+"] into " + reg);
    	
    	//Load all to the memory location before sto[nIndex]
    	//e.g. a.b.c.e[3] will load up to a.b.c.e[0] 
    	loadBaseOffset(sto, sto.getbArray());
    	
		
    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL4 + "]", reg);
	}
	
	private void loadBaseOffset(STO memberSTO, STO baseSTO) {
		
		if (baseSTO.getIsArray())
		{
			writeComment("Recursive Call to get Offset of Array " + baseSTO.getbArray().getName());
			loadBaseOffset(baseSTO, baseSTO.getbArray());
		}
		else if(baseSTO.getIsStruct())
		{
			writeComment("Recursive Call to get Offset of Struct " + baseSTO.getbStruct().getName());
			loadBaseOffset(baseSTO, baseSTO.getbStruct());
		}
		else if (baseSTO.getIsDeref())
		{
			writeComment("Recursive Call to get Offset of Pointer " + baseSTO.getbPointer().getName());
			loadBaseOffset(baseSTO, baseSTO.getbPointer());
		}

		
		if (baseSTO.getType().isArray() || (baseSTO.getIsDeref() && memberSTO.getnIndex() != null))
		{
			writeComment("Get Offset of var " + baseSTO.getName()+"["+memberSTO.getnIndex().getName()
					+"]" +" and Store into %l4");
			
	    	STO nIndex = memberSTO.getnIndex();
	    	
	    	//load index
			if (nIndex.isConst() && nIndex.getAsmName().equals(""))
			{
				loadLiteralInReg(nIndex, SparcInstr.REG_LOCAL0);
			}
			else
			{
				loadStackInReg(nIndex, SparcInstr.REG_LOCAL0);
			}
			
			if (memberSTO.getType().getSize() ==4)
			{
				
				writeAssembly(SparcInstr.THREE_PARAM_COMM, SparcInstr.SLL_OP, SparcInstr.REG_LOCAL0, "2", SparcInstr.REG_LOCAL0,
						nIndex.getName()+" * 4 -> scaled offset");
			}
			else
			{
				//size of struct
				writeComment("Shift by size of struct " + memberSTO.getType().getSize());
				writeComment("Move reg %o0 and %o1 to %l5 and %l6 in case %o0 is in use");
				MoveRegToReg(SparcInstr.REG_OUTPUT0, SparcInstr.REG_LOCAL5);
				MoveRegToReg(SparcInstr.REG_OUTPUT1, SparcInstr.REG_LOCAL6);
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, ""+memberSTO.getType().getSize(), SparcInstr.REG_OUTPUT0);
				MoveRegToReg(SparcInstr.REG_LOCAL0, SparcInstr.REG_OUTPUT1);
				writeCallFuncName(SparcInstr.MUL_OP);
				
				writeComment("Move scaled offset back to %l0");
				MoveRegToReg(SparcInstr.REG_OUTPUT0, SparcInstr.REG_LOCAL0);
				writeAssembly(SparcInstr.BLANK_LINE);
				
				writeComment("Move back %o0, %o1");
				MoveRegToReg(SparcInstr.REG_LOCAL5, SparcInstr.REG_OUTPUT0);
				MoveRegToReg(SparcInstr.REG_LOCAL6, SparcInstr.REG_OUTPUT1);
				writeAssembly(SparcInstr.BLANK_LINE);
				
			}
			
			if (baseSTO.getBase().equals("") && baseSTO.getOffset().equals("") || baseSTO.getIsDeref())
			{
				if (memberSTO.getIsStatic() || memberSTO.getIsGlobal())
				{
					writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL4);
				}
				else
				{
					writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL4);
				}
			}
			else
			{
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, baseSTO.getOffset(), SparcInstr.REG_LOCAL3);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, baseSTO.getBase(), SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL3);
		    	if (memberSTO.getIsRef())
		    	{
		    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL3 + "]", SparcInstr.REG_LOCAL3);
		    	}
				if (memberSTO.getIsStatic() || memberSTO.getIsGlobal())
				{
					writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL4);
				}
				else
				{
					writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL4);
				}
			}
		}
		else if (baseSTO.getType().isStruct() || (baseSTO.getIsDeref() && baseSTO.getType().isStruct()))
		{
			writeComment("Get Offset of var " + baseSTO.getName()+"."+memberSTO.getName() +" and Store into %l4");
			
	    	StructType sType = (StructType)baseSTO.getType();
	    	Vector<STO> members = sType.getMemeber();
	    	
	    	int offset = 0;
	    	
	    	for (STO member: members)
	    	{
	    		if (memberSTO.getName().equals(member.getName()))
	    		{
	    			if ((baseSTO.getBase().equals("") && baseSTO.getOffset().equals("")) || baseSTO.getIsDeref())
	    			{
				    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, offset+"", SparcInstr.REG_LOCAL3);
						if (memberSTO.getIsStatic() || memberSTO.getIsGlobal())
						{
							writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL4);
						}
						else
						{
							writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL4);
						}
	    			}
	    			else
	    			{
				    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, baseSTO.getOffset(), SparcInstr.REG_LOCAL0);
				    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, baseSTO.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
				    	if (memberSTO.getIsRef())
				    	{
				    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
				    	}
				    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, offset+"", SparcInstr.REG_LOCAL3);
				    	
						if (memberSTO.getIsStatic() || memberSTO.getIsGlobal())
						{
							writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL4);
	    			
						}
						else
						{
							writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL3, SparcInstr.REG_LOCAL4);
						}
	    			}
	    		}
	    		else
	    		{
	    			offset+=member.getType().getSize();
	    		}
	    	}
		}
		else if(baseSTO.getType().isPointer())
		{
			writeComment("Load Address that " + baseSTO.getName()+" points to and Store into %l4");
			if (baseSTO.getIsStruct())
			{
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL4 + "]", SparcInstr.REG_LOCAL4);
			}
			else
			{
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, baseSTO.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, baseSTO.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL4);
		    	if (memberSTO.getIsRef())
		    	{
		    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL4 + "]", SparcInstr.REG_LOCAL4);
		    	}
			}
		}
	}
    
	public void loadMemInReg(STO sto, String reg)
	{
		if (sto.getIsRef())
    	{
    		if (!sto.getIsArray() && !sto.getIsStruct())
    		{
	    		writeComment("Load memory of reference " + sto.getName() + " into " + reg);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", reg);
    		}
    		else if (sto.getIsArray())
    		{
    			loadBaseOffset(sto, sto.getbArray());
    			MoveRegToReg(SparcInstr.REG_LOCAL4, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			loadBaseOffset(sto, sto.getbStruct());
    			MoveRegToReg(SparcInstr.REG_LOCAL4, reg);
    		}
    		
    	}
    	else
    	{
    		writeComment("Load memory of " + sto.getName() + " into " + reg);
    		if (!sto.getIsArray() && !sto.getIsStruct())
    		{
		    	writeComment("Load " + sto.getName() + " into " + reg);
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, sto.getOffset(), SparcInstr.REG_LOCAL0);
		    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, sto.getBase(), SparcInstr.REG_LOCAL0, reg);
    		}
    		else if (sto.getIsArray())
    		{
    			loadBaseOffset(sto, sto.getbArray());
    			MoveRegToReg(SparcInstr.REG_LOCAL4, reg);
    		}
    		else if (sto.getIsStruct())
    		{
    			loadBaseOffset(sto, sto.getbStruct());
    			MoveRegToReg(SparcInstr.REG_LOCAL4, reg);
    		}
    	}
    	writeAssembly(SparcInstr.BLANK_LINE);
	}
	
	//-------------------------------------------------------------------------
    //     Switch to section and align
    //-------------------------------------------------------------------------
    public void doSwitchSection(String section, String align)
    {
    	// .section
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.SECTION_DIR, section);
        // .align 
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.ALIGN_DIR, align);
	
    }
    
    //-------------------------------------------------------------------------
    //     HelperFunction to write the id: .word / id: .single0r
    //-------------------------------------------------------------------------
    public void writeVarInit(STO varSto, STO valueSTO, String finalName)
    {
        // <id>: .skip size of type of varSto
        decreaseIndent();

        //no initialization
        if (valueSTO.isEmpty())
        {
			if(varSto.getType().isInt())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.WORD_DIR, "0");
			}
			if(varSto.getType().isFloat())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.SINGLE_DIR, "0");
			}
			if(varSto.getType().isBool())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName,  SparcInstr.WORD_DIR, "0");
			}
			if(varSto.getType().isArray())
			{
				ArrayType ar = ((ArrayType)varSto.getType());
				writeComment("Array Type: size of " + ar.getBaseType().getName() +" "+ ar.getBaseType().getSize()+ " * " +
						((ArrayType)varSto.getType()).getNumOfElement());
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName,  SparcInstr.SKIP_DIR, varSto.getType().getSize()+"");
			}
			if (varSto.getType().isStruct())
			{
				writeComment("Struct Type: size of " +varSto.getType().getSize());
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName,  SparcInstr.SKIP_DIR, varSto.getType().getSize()+"");
			}
			if (varSto.getType().isPointer() || varSto.getType().isFunctionPointer())
			{
				writeComment("Pointer Type");
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName,  SparcInstr.SKIP_DIR, varSto.getType().getSize()+"");
			}
        }
        else
        {
			if(varSto.getType().isInt() && !varSto.getIsString())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.WORD_DIR, ((ConstSTO)valueSTO).getIntValue()+"");
			}
			if(varSto.getType().isFloat())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.SINGLE_DIR, ((ConstSTO)valueSTO).getFloatValue()+"");
			}
			if(varSto.getType().isBool())
			{
				String bool = ((ConstSTO)valueSTO).getBoolValue()?"1":"0";
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.WORD_DIR, bool);
			}
			if(varSto.getType().isInt() && varSto.getIsString())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.ASCIZ_DIR, "\""+varSto.getName()+"\"");
			}
			if ((varSto.getType().isPointer()||varSto.getType().isFunctionPointer()) && valueSTO.getType().isNullPointer())
			{
				writeAssembly(SparcInstr.GLOBAL_DEFINE, finalName, SparcInstr.WORD_DIR, 0+"");
			}
			if ((varSto.getType().isFunctionPointer() && valueSTO.getType().isFunctionPointer()))
			{
				writeAssembly(SparcInstr.ONE_PARAM, finalName+":", valueSTO.getName());
			}
			
        }
        
        increaseIndent();
    }
         
    //-------------------------------------------------------------------------
    //     HelperFunction to change bit pattern
    //-------------------------------------------------------------------------
    public void changeBitPattern(String op, String regFrom, String regTo)
    {
		writeAssembly(SparcInstr.TWO_PARAM_COMM, op, regFrom, regTo,
				"Change bit pattern");
		writeAssembly(SparcInstr.BLANK_LINE);
    }
       
    //-------------------------------------------------------------------------
    //     Arithmatic Integer Expr
    //-------------------------------------------------------------------------
	public void DoArithmaticExpr(STO left, String o, STO right, STO result) {
		
		if (MyParser.isDeclType)
			return;
		
		//if the result is not a const, do the assemly code to get the result
		//store the result to a local stack
		if (!result.isConst() && !getFuncName().equals("global"))
		{
			String binaryOp = "";
			String RegOp1 = SparcInstr.REG_LOCAL1;
			String RegOp2 = SparcInstr.REG_LOCAL2;
			boolean callOp = false;
			
			//if left or right is a float, goto float expr function
			if (left.getType().isFloat() || right.getType().isFloat())
			{
				DoArithmaticFloatExpr(left, o, right, result);
				return;
			}
			
			//load Left hand side to stack
			if (left.isConst() && !left.getIsAddressable())
			{
				loadLiteralInReg(left, RegOp1);
			}
			else
			{
				loadStackInReg(left, RegOp1);
			}
			
			//load right hand side to stack
			if (right.isConst() && !right.getIsAddressable())
			{
				loadLiteralInReg(right, RegOp2);
			}
			else
			{
				loadStackInReg(right, RegOp2);
			}
			
			writeComment(left.getName() +" " + o + " " +right.getName());
			//Then do arithmatic expression
			if (o.equals("+"))
			{
				binaryOp = SparcInstr.ADD_OP;
			}
			else if (o.equals("-"))
			{
				binaryOp = SparcInstr.SUB_OP;
			}
			else if (o.equals("*"))
			{
				binaryOp = SparcInstr.MUL_OP;
				callOp = true;
			}
			else if (o.equals("/"))
			{
				binaryOp = SparcInstr.DIV_OP;
				callOp = true;
			}
			else if (o.equals("%"))
			{
				binaryOp = SparcInstr.REM_OP;
				callOp = true;
			}
			else if (o.equals("^"))
			{
				binaryOp = SparcInstr.XOR_OP;
			}
			else if (o.equals("&"))
			{
				binaryOp = SparcInstr.AND_OP;
			}
			else if (o.equals("|"))
			{
				binaryOp = SparcInstr.OR_OP;
			}
			
			if (callOp)
			{
				//move reg1/2 to %o0/1
                MoveRegToReg(RegOp1, SparcInstr.REG_ARG0);
                MoveRegToReg(RegOp2, SparcInstr.REG_ARG1);
                
                //call function
                //nop
                writeCallFuncName(binaryOp);
                /*
                writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.CALL_OP, binaryOp);
                writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
                writeAssembly(SparcInstr.BLANK_LINE);
                */
                //mov %o0, reg1
                MoveRegToReg(SparcInstr.REG_GET_RETURN, RegOp1);
				
			}
			else
			{
				//instr	reg1, reg2, reg1
				writeAssembly(SparcInstr.THREE_PARAM, binaryOp, RegOp1, RegOp2, RegOp1);
			}
			
			//Store result into %fp - x
			int offset = getNextOffset();
			
			//store offset and base 
			result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		
			storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			writeAssembly(SparcInstr.BLANK_LINE);
			
		}
		
	}
	
	 //-------------------------------------------------------------------------
	 //     Arithmatic Float Expr
	 //-------------------------------------------------------------------------
    private void DoArithmaticFloatExpr(STO left, String o, STO right, STO result) {
		
    	String binaryOp = "";
		String RegOp1 = SparcInstr.REG_FLOAT0;
		String RegOp2 = SparcInstr.REG_FLOAT1;
		
		if (left.isConst() && left.getAsmName().equals(""))
		{
			loadFloatORIntInReg(left, RegOp1);
		}
		else
		{
			loadStackInReg(left, RegOp1);
			
			//change bit pattern for int
			if (left.getType().isInt())
			{
				changeBitPattern(SparcInstr.FITOS_OP, RegOp1, RegOp1);
			}
				
		}
		
		//load right hand side to stack
		if (right.isConst() && right.getAsmName().equals(""))
		{
			loadFloatORIntInReg(right, RegOp2);
		}
		else
		{
			loadStackInReg(right, RegOp2);
			
			//change bit pattern for int
			if (right.getType().isInt())
			{
				changeBitPattern(SparcInstr.FITOS_OP, RegOp2, RegOp2);
			}
		}
		
		writeComment(left.getName() +" " + o + " " +right.getName());
		//Then do arithmatic expression
		if (o.equals("+"))
		{
			binaryOp = SparcInstr.FADDS_OP;
		}
		else if (o.equals("-"))
		{
			binaryOp = SparcInstr.FSUBS_OP;
		}
		else if (o.equals("*"))
		{
			binaryOp = SparcInstr.FMULS_OP;
		}
		else if (o.equals("/"))
		{
			binaryOp = SparcInstr.FDIVS_OP;
		}
		

		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.THREE_PARAM, binaryOp, RegOp1, RegOp2, RegOp1);
		
		//Store result into %fp - x
		int offset = getNextOffset();
		
		//store offset and base 
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
	
		storeRegValueInMem(result, RegOp1);
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
    
	//-------------------------------------------------------------------------
    //     Move the reg to another reg
    //-------------------------------------------------------------------------
	private void MoveRegToReg(String valueReg, String destReg) {
        writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.MOV_OP, valueReg, destReg, "Moving value in " + valueReg  + " into  " + destReg);
	}
		
    //-------------------------------------------------------------------------
    //     Boolean Expr
    //-------------------------------------------------------------------------
	public void DoBooleanExpr(STO a, String o, STO b, STO result) {
		if (MyParser.isDeclType)
			return;
		
		if (!result.isConst() && !getFuncName().equals("global"))
		{
			if (a.getType().isInt() && b.getType().isInt())
			{
				DoIntBooleanExpr(a, o, b, result);
			}
			else if (a.getType().isFloat() || b.getType().isFloat())
			{
				DoFloatBooleanExpr(a, o, b, result);
			}
			else if (a.getType().isBool() && b.getType().isBool())
			{
				DoBoolBooleanExpr(a, o, b, result);
			}
			else if (a.getType().isPointer() && b.getType().isPointer())
			{
				DoPointerBooleanExpr(a, o, b, result);
			}
		}
		else
		{
			
			if (o.equals("||"))
			{
				int num = getAndOrStack().lastElement();
				getAndOrStack().removeElementAt(getAndOrStack().size()-1);
				
				decreaseIndent();
				decreaseIndent();
				writeComment("Constant Folding");
				writeAssembly(SparcInstr.LABEL, "end_or_"+(num));
				increaseIndent();
				increaseIndent();
			}
			if (o.equals("&&"))
			{
				int num = getAndOrStack().lastElement();
				getAndOrStack().removeElementAt(getAndOrStack().size()-1);
				
				decreaseIndent();
				decreaseIndent();
				writeComment("Constant Folding");
				writeAssembly(SparcInstr.LABEL, "end_and_"+(num));
				increaseIndent();
				increaseIndent();
			}
		}
	}
	
	private void DoPointerBooleanExpr(STO a, String o, STO b, STO result) {
		
		String RegOp1 = SparcInstr.REG_LOCAL5;
		String RegOp2 = SparcInstr.REG_LOCAL6;
		String binaryOp= "";
		
		if (a.getType().isNullPointer())
		{
			RegOp1 = SparcInstr.REG_GLOBAL0;
		}
		else
		{
			loadStackInReg(a, SparcInstr.REG_LOCAL5);
		}
		
		if (b.getType().isNullPointer())
		{
			RegOp2 = SparcInstr.REG_GLOBAL0;
		}
		else
		{
			loadStackInReg(b, SparcInstr.REG_LOCAL6);
		}
		
		if (o.equals("=="))
		{
			binaryOp = SparcInstr.BE_OP;
		}
		else if (o.equals("!="))
		{
			binaryOp = SparcInstr.BNE_OP;
		}		
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, RegOp1, RegOp2
				,"Compare " + RegOp1 + " with " + RegOp2);
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.ONE_PARAM_COMM, binaryOp, "true_"+bool_num 
				,"Go To true_"+bool_num +" if "+binaryOp);
		writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		
		//False
		writeComment("Below is the false condition");
		loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
		writeAssembly(SparcInstr.NOP_OP);		            
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		//True
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "true_"+bool_num);
		increaseIndent();
		increaseIndent();
		loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
		
		//End
		//Store retult into memory stack
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
		increaseIndent();
		increaseIndent();
		
		bool_num++;
	
	
		//Store result into %fp - x
		int offset = getNextOffset();
		
		//store offset and base 
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
	
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		
		
	}
	
	//-------------------------------------------------------------------------
    //     Bool &&\|| Bool
    //-------------------------------------------------------------------------
	public void DoAndOrExpr(STO a, String o)
	{
		String binaryOp = "";
		String RegOp1 = SparcInstr.REG_LOCAL5;
		String RegOp2 = SparcInstr.REG_LOCAL6;

		writeComment("EXPR: "+a.getName() +" "+o+" => check left hand side" );
		//Then do arithmatic expression

		if (o.equals("&&") || o.equals("||"))
		{
			if (a.isConst()&& a.getAsmName().equals(""))
			{
				if (o.equals("&&") && ((ConstSTO)a).getIntValue() ==0)
				{
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_and_"+and_num);
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
					writeAssembly(SparcInstr.BLANK_LINE);
					addAndOrNum(and_num);
					and_num++;
					return;
				}
				if (o.equals("||") && ((ConstSTO)a).getIntValue() ==1)
				{
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_or_"+or_num);
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
					writeAssembly(SparcInstr.BLANK_LINE);
					addAndOrNum(or_num);
					or_num++;
					return;
				}
			}

			//load Left hand side to stack
			if (a.isConst() && a.getAsmName().equals(""))
			{
				writeBooleanToIntAsm(a, RegOp1);
			}
			else
			{
				loadStackInReg(a, RegOp1);
			}
			
			if (o.equals("&&"))
			{
				writeComment("&&: if "+ a.getName()+" is false, do not have to evaluate RHS");
				//instr	reg1, reg2, reg1
				writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp1
						,"Compare %g0 with " + RegOp1);
				
				//instr	reg1, reg2, reg1
				writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BNE_OP, "true_and_"+and_num 
						,"Go to true_andor if "+ a.getName() +" is true");
				writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        
				
				//False
				writeComment("Below is the False condition");
				loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_and_"+and_num);
				writeAssembly(SparcInstr.NOP_OP);		            
		        writeAssembly(SparcInstr.BLANK_LINE);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        
				//True
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "true_and_"+and_num);
				increaseIndent();
				increaseIndent();
				
				addAndOrNum(and_num);
				and_num++;	
			}
			else
			{
				writeComment("||: if "+ a.getName()+" is true, do not have to evaluate RHS" );
				
				//instr	reg1, reg2, reg1
				writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp1
						,"Compare %g0 with " + RegOp1);
				
				//instr	reg1, reg2, reg1
				writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BE_OP, "false_or_"+or_num 
						,"Go to  L false_andor if "+ a.getName() +" is false");
				writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        
				
				//True
				writeComment("Below is the L True condition");
				loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_or_"+or_num);
				writeAssembly(SparcInstr.NOP_OP);		            
		        writeAssembly(SparcInstr.BLANK_LINE);
		        writeAssembly(SparcInstr.BLANK_LINE);
		        
				//False
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "false_or_"+or_num);
				increaseIndent();
				increaseIndent();
				
				addAndOrNum(or_num);
				or_num++;
			}

		}
	}
	
	//-------------------------------------------------------------------------
    //     Bool boolOP Bool
    //-------------------------------------------------------------------------
    private void DoBoolBooleanExpr(STO a, String o, STO b, STO result) {
		String binaryOp = "";
		String RegOp1 = SparcInstr.REG_LOCAL5;
		String RegOp2 = SparcInstr.REG_LOCAL6;
		
		if (and_num == 12)
		{
			and_num = 12;
		}

		writeComment(a.getName() +" " + o + " " +b.getName());
		//Then do arithmatic expression

		if (o.equals("&&") || o.equals("||"))
		{
			int num = getAndOrStack().lastElement();
			getAndOrStack().removeElementAt(getAndOrStack().size()-1);
			
			if (a.isConst()&& a.getAsmName().equals(""))
			{
				if (o.equals("&&") && ((ConstSTO)a).getIntValue() ==0)
				{
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "end_and_"+num);
					increaseIndent();
					increaseIndent();
					//Store result into %fp - x
					int offset = getNextOffset();
					
					//store offset and base 
					result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
				
					storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
					writeAssembly(SparcInstr.BLANK_LINE);
					return;
				}
				if (o.equals("||") && ((ConstSTO)a).getIntValue() ==1)
				{
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "end_or_"+num);
					increaseIndent();
					increaseIndent();
					//Store result into %fp - x
					int offset = getNextOffset();
					
					//store offset and base 
					result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
				
					storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
					writeAssembly(SparcInstr.BLANK_LINE);
					return;
				}
			}
			/*
			if (a.isConst()&& a.getAsmName().equals(""))
			{
				if (o.equals("&&") && ((ConstSTO)a).getIntValue() ==0)
				{
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
				}
				if (o.equals("||") && ((ConstSTO)a).getIntValue() ==1)
				{
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
				}
			}
			else if (b.isConst()&& b.getAsmName().equals(""))
			{
				if (o.equals("&&") && ((ConstSTO)b).getIntValue() ==0)
				{
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
				}
				if (o.equals("||") && ((ConstSTO)b).getIntValue() ==1)
				{
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
				}
			}
			else
			{
				//load Left hand side to stack
				if (a.isConst() && a.getAsmName().equals(""))
				{
					writeBooleanToIntAsm(a, RegOp1);
				}
				else
				{
					loadStackInReg(a, RegOp1);
				}
				
				if (o.equals("&&"))
				{
					writeComment("&&: if "+ a.getName()+" is false, do not have to evaluate " + b.getName());
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp1
							,"Compare %g0 with " + RegOp1);
					
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BNE_OP, "true_andor_"+bool_num 
							,"Go to true_andor if "+ a.getName() +" is true");
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					
					//False
					writeComment("Below is the False condition");
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
					
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_andor_"+bool_num);
					writeAssembly(SparcInstr.NOP_OP);		            
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					//True
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "true_andor_"+bool_num);
					increaseIndent();
					increaseIndent();
					
					bool_num++;		
				}
				else
				{
					writeComment("||: if "+ a.getName()+" is true, do not have to evaluate " + b.getName());
					
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp1
							,"Compare %g0 with " + RegOp1);
					
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BE_OP, "false_andor_"+bool_num 
							,"Go to false_andor if "+ a.getName() +" is false");
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					
					//True
					writeComment("Below is the True condition");
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
					
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_andor_"+bool_num);
					writeAssembly(SparcInstr.NOP_OP);		            
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					//False
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "false_andor_"+bool_num);
					increaseIndent();
					increaseIndent();
					
					bool_num++;
				}
				*/
				//load right hand side to stack
				if (b.isConst() && b.getAsmName().equals(""))
				{
					writeBooleanToIntAsm(b, RegOp2);
				}
				else
				{
					loadStackInReg(b, RegOp2);
				}
				if (o.equals("&&"))
				{
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp2
							,"Compare %g0 with " + RegOp1);
					
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BE_OP, "false_and_"+and_num 
							,"Go to R false_and if "+ b.getName() +"is false");
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					
					//True
					writeComment("Below is the R True condition");
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
					
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_and_"+ num );
					writeAssembly(SparcInstr.NOP_OP);		            
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					//False
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "false_and_"+and_num);
					increaseIndent();
					increaseIndent();
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
			        
			        
					//End
					//Store retult into memory stack
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "end_and_"+num);
					increaseIndent();
					increaseIndent();
					
					and_num++;
				}
				else
				{
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, RegOp2
							,"Compare %g0 with " + RegOp1);
					
					//instr	reg1, reg2, reg1
					writeAssembly(SparcInstr.ONE_PARAM_COMM, SparcInstr.BE_OP, "false_or_"+or_num 
							,"Go to false_or if "+ b.getName() +"is false");
					writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					
					//True
					writeComment("Below is the True condition");
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
					
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_or_"+ num );
					writeAssembly(SparcInstr.NOP_OP);		            
			        writeAssembly(SparcInstr.BLANK_LINE);
			        writeAssembly(SparcInstr.BLANK_LINE);
			        
					//False
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "false_or_"+or_num);
					increaseIndent();
					increaseIndent();
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
			        
			        
					//End
					//Store retult into memory stack
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "end_or_"+num);
					increaseIndent();
					increaseIndent();
					
					or_num++;
				}
			
			//}
		}
		else
		{
			//load Left hand side to stack
			if (a.isConst() && a.getAsmName().equals(""))
			{
				writeBooleanToIntAsm(a, RegOp1);
			}
			else
			{
				loadStackInReg(a, RegOp1);
			}
			
			//load right hand side to stack
			if (b.isConst() && b.getAsmName().equals(""))
			{
				writeBooleanToIntAsm(b, RegOp2);
			}
			else
			{
				loadStackInReg(b, RegOp2);
			}
			
			if (o.equals("=="))
			{
				binaryOp = SparcInstr.BE_OP;
			}
			else if (o.equals("!="))
			{
				binaryOp = SparcInstr.BNE_OP;
			}		
			
			//instr	reg1, reg2, reg1
			writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, RegOp1, RegOp2
					,"Compare " + RegOp1 + " with " + RegOp2);
			
			//instr	reg1, reg2, reg1
			writeAssembly(SparcInstr.ONE_PARAM_COMM, binaryOp, "true_"+bool_num 
					,"Go To true_"+bool_num +" if "+binaryOp);
			writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
	        writeAssembly(SparcInstr.BLANK_LINE);
	        writeAssembly(SparcInstr.BLANK_LINE);
	        
			
			//False
			writeComment("Below is the false condition");
			loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
			
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
			writeAssembly(SparcInstr.NOP_OP);		            
	        writeAssembly(SparcInstr.BLANK_LINE);
	        writeAssembly(SparcInstr.BLANK_LINE);
	        
			//True
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "true_"+bool_num);
			increaseIndent();
			increaseIndent();
			loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
			
			//End
			//Store retult into memory stack
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
			increaseIndent();
			increaseIndent();
			
			bool_num++;
		}
		
		//Store result into %fp - x
		int offset = getNextOffset();
		
		//store offset and base 
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
	
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		
		
	}
    
	//-------------------------------------------------------------------------
    //     Float boolOP Float
    //-------------------------------------------------------------------------
	private void DoFloatBooleanExpr(STO a, String o, STO b, STO result) {
		
		
		String binaryOp = "";
		String RegOp1 = SparcInstr.REG_FLOAT0;
		String RegOp2 = SparcInstr.REG_FLOAT1;
		
		//load Left hand side to stack
		if (a.isConst() && a.getAsmName().equals(""))
		{
			loadFloatORIntInReg(a, RegOp1);
		}
		else
		{
			loadStackInReg(a, RegOp1);
			
			//change bit pattern for int
			if (a.getType().isInt())
			{
				changeBitPattern(SparcInstr.FITOS_OP, RegOp1, RegOp1);
			}
		}
		
		//load right hand side to stack
		if (b.isConst() && b.getAsmName().equals(""))
		{
			loadFloatORIntInReg(b, RegOp2);
		}
		else
		{
			loadStackInReg(b, RegOp2);
			
			//change bit pattern for int
			if (b.getType().isInt())
			{
				changeBitPattern(SparcInstr.FITOS_OP, RegOp2, RegOp2);
			}
		}
		
		writeComment(a.getName() +" " + o + " " +b.getName());
		//Then do arithmatic expression
		if (o.equals(">"))
		{
			binaryOp = SparcInstr.FBG_OP;
		}
		else if (o.equals(">="))
		{
			binaryOp = SparcInstr.FBGE_OP;
		}
		else if (o.equals("<"))
		{
			binaryOp = SparcInstr.FBL_OP;
		}
		else if (o.equals("<="))
		{
			binaryOp = SparcInstr.FBLE_OP;
		}
		else if (o.equals("=="))
		{
			binaryOp = SparcInstr.FBE_OP;
		}
		else if (o.equals("!="))
		{
			binaryOp = SparcInstr.FBNE_OP;
		}
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.FCMPS_OP, RegOp1, RegOp2
				,"Compare " + RegOp1 + " with " + RegOp2);
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.ONE_PARAM_COMM, binaryOp, "true_"+bool_num 
				,"Go To true_"+bool_num +" if "+binaryOp);
		writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		
		//False
		writeComment("Below is the false condition");
		loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
		writeAssembly(SparcInstr.NOP_OP);		            
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		//True
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "true_"+bool_num);
		increaseIndent();
		increaseIndent();
		loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
		
		//End
		//Store retult into memory stack
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
		increaseIndent();
		increaseIndent();
		
		//Store result into %fp - x
		int offset = getNextOffset();
		
		//store offset and base 
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
	
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		bool_num++;
		
	}
	
	//-------------------------------------------------------------------------
    //     Int boolOP Int
    //-------------------------------------------------------------------------
	private void DoIntBooleanExpr(STO a, String o, STO b, STO result) {
		
		String binaryOp = "";
		String RegOp1 = SparcInstr.REG_OUTPUT0;
		String RegOp2 = SparcInstr.REG_OUTPUT1;
		
		//load Left hand side to stack
		if (a.isConst() && a.getAsmName().equals(""))
		{
			loadLiteralInReg(a, RegOp1);
		}
		else
		{
			loadStackInReg(a, RegOp1);
		}
		
		//load right hand side to stack
		if (b.isConst() && b.getAsmName().equals(""))
		{
			loadLiteralInReg(b, RegOp2);
		}
		else
		{
			loadStackInReg(b, RegOp2);
		}
		
		writeComment(a.getName() +" " + o + " " +b.getName());
		//Then do arithmatic expression
		if (o.equals(">"))
		{
			binaryOp = SparcInstr.BG_OP;
		}
		else if (o.equals(">="))
		{
			binaryOp = SparcInstr.BGE_OP;
		}
		else if (o.equals("<"))
		{
			binaryOp = SparcInstr.BL_OP;
		}
		else if (o.equals("<="))
		{
			binaryOp = SparcInstr.BLE_OP;
		}
		else if (o.equals("=="))
		{
			binaryOp = SparcInstr.BE_OP;
		}
		else if (o.equals("!="))
		{
			binaryOp = SparcInstr.BNE_OP;
		}
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.CMP_OP, RegOp1, RegOp2
				,"Compare " + RegOp1 + " with " + RegOp2);
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.ONE_PARAM_COMM, binaryOp, "true_"+bool_num 
				,"Go To true_"+bool_num +" if "+binaryOp);
		writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		
		//False
		writeComment("Below is the false condition");
		loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
		writeAssembly(SparcInstr.NOP_OP);		            
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        
		//True
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "true_"+bool_num);
		increaseIndent();
		increaseIndent();
		loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
		
		//End
		//Store retult into memory stack
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
		increaseIndent();
		increaseIndent();
		
		//Store result into %fp - x
		int offset = getNextOffset();
		
		//store offset and base 
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
	
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		bool_num++;
			
	}
	
	//-------------------------------------------------------------------------
    //     Cout
    //-------------------------------------------------------------------------
	public void DoCout(STO sto, boolean isConst, boolean isEndl)
	{
		writeComment("Cout " + sto.getName());
		
		//ENDL
		if (sto.isEmpty() && isEndl)
		{
			loadFormatInReg(SparcInstr.ENDL, SparcInstr.REG_OUTPUT0);
			writeCallFuncName(SparcInstr.PRINTF);
		}
		else
		{
			//print int
			if (sto.getType().isInt() && !sto.getIsString() || sto.getType().isPointer())
			{
				//set	_IntFmt, %o0
				loadFormatInReg(SparcInstr.INTFMT, SparcInstr.REG_OUTPUT0);
				
				//set integer to %o1
				//if it is literal
				if (isConst && !sto.getIsAddressable())
				{
					loadLiteralInReg(sto, SparcInstr.REG_OUTPUT1);	
				}
				else
				{
					loadStackInReg(sto, SparcInstr.REG_OUTPUT1);
				}
				writeCallFuncName(SparcInstr.PRINTF);
			}
			else if (sto.getType().isFloat())		//FLOAT
			{
				if(isConst && !sto.getIsAddressable())
				{
					String finalName = "zachfloat" + "_";
		        	if (sto.getName().charAt(0) == '-')
		        		finalName+=  "n"+sto.getName().substring(1);
		        	else
		        		finalName+=  sto.getName();
		    		
		    		writeComment("Declare " + sto.getName() + " at the end of function");
		                          
		            if (!CheckExistStaticGlobal(finalName))
		            {
			            //if it does not exist in the stack yet
			            //Add Global/Static Variable to Stack 
		            	
		            	
			            addStaticGlobal(sto);
			            addLocalStatic(sto);
			            addLocalStaticValue(sto);
		            }
		        	sto.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
		

				}
					writeComment("Need to clr %o0 %o1 %l3, does not know reason");
				    writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.CLR_OP, SparcInstr.REG_OUTPUT0);	 
				    writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.CLR_OP, SparcInstr.REG_OUTPUT1);
				    writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.CLR_OP, SparcInstr.REG_LOCAL3);
				
					loadStackInReg(sto, SparcInstr.REG_FLOAT0);
		         	writeCallFuncName(SparcInstr.PRINTFLOAT);		            
		            writeAssembly(SparcInstr.BLANK_LINE);	  
			}
			else if(sto.getType().isBool())
			{
				if(isConst && !sto.getIsAddressable())
				{
					//if it's a true const
					if(sto.getName().equals("true"))
					{
						loadFormatInReg(SparcInstr.BOOLT, SparcInstr.REG_OUTPUT1);
					}
					//false const
					else
					{
						loadFormatInReg(SparcInstr.BOOLF, SparcInstr.REG_OUTPUT1);
					}
				}
				else
				{
					loadStackInReg(sto, SparcInstr.REG_OUTPUT1);
					writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_OUTPUT1 );
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "false_"+bool_num);
					writeAssembly(SparcInstr.NOP_OP);		        
		            writeAssembly(SparcInstr.BLANK_LINE);
					loadFormatInReg(SparcInstr.BOOLT, SparcInstr.REG_OUTPUT1);
					writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
					writeAssembly(SparcInstr.NOP_OP);		            
		            writeAssembly(SparcInstr.BLANK_LINE);
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "false_"+bool_num);
					increaseIndent();
					increaseIndent();
					loadFormatInReg(SparcInstr.BOOLF, SparcInstr.REG_OUTPUT1);
					decreaseIndent();
					decreaseIndent();
					writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
					increaseIndent();
					increaseIndent();
					bool_num++;
					
				}
				loadFormatInReg(SparcInstr.STRFMT, SparcInstr.REG_OUTPUT0);
				writeCallFuncName(SparcInstr.PRINTF);
			}
			else if (sto.getIsString())				//STRING
			{
	    		String finalName = getFuncName() + "_str_" + str_num;	
	    		
	    		writeComment("Declare String \"" + sto.getName() + "\" at the end of function");
	                          
	            if (!CheckExistStaticGlobal(finalName))
	            {
		            //if it does not exist in the stack yet
		            //Add Global/Static Variable to Stack 
	            	
	            	sto.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
	            	
		            addStaticGlobal(sto);
		            addLocalStatic(sto);
		            addLocalStaticValue(sto);
	            }
	            
	            loadFormatInReg(finalName, SparcInstr.REG_OUTPUT1);
	            loadFormatInReg(SparcInstr.STRFMT, SparcInstr.REG_OUTPUT0);
				
	            writeCallFuncName(SparcInstr.PRINTF);
	            
	            writeAssembly(SparcInstr.BLANK_LINE);
	            
	            str_num++;
			}
		}

	}
	
    //-------------------------------------------------------------------------
    //     call	functionName
	//	   nop
    //-------------------------------------------------------------------------
	public void writeCallFuncName(String funcName)
	{
        writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.CALL_OP, funcName);
        writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
        writeAssembly(SparcInstr.BLANK_LINE);
	}
	
    //-------------------------------------------------------------------------
    //     Check if the variable exist in the data section
    //-------------------------------------------------------------------------
	private boolean CheckExistStaticGlobal(String finalName)
	{
        //find if static variable already declare
        Vector<STO> sgStack = getStaticGlobal();
        for(STO sto: sgStack)
        {
        	if (sto.getAsmName().equals(finalName))
        	{
        		return true;
        	}
        	
        }
        return false;
	}
	
    //-------------------------------------------------------------------------
    //     x = something for x is int/float/bool 
    //-------------------------------------------------------------------------
	public void DoAssign(STO varSto, STO valueSTO)
	{
		if (MyParser.isDeclType)
			return;
		
		if (varSto.getType().isStruct())
		{
			DoAssignStruct(varSto, valueSTO);
			return;
		}
		if (varSto.getType().isFunctionPointer() && valueSTO.getType().isFunctionPointer())
		{
			DoAssignFP(varSto, valueSTO);
			return;
		}
		
		
		Type vartyp = varSto.getType();
		Type valuetyp = valueSTO.getType();
		
		//if right expr is a const
		//e.g. int x = 1; int y= 1+2+3;
		if (valueSTO.isConst() && valueSTO.getAsmName().equals(""))
		{
			//int with int
			if (valuetyp.isInt() && vartyp.isInt())
			{
				loadLiteralInReg(valueSTO, SparcInstr.REG_LOCAL1);
				writeComment("Store " + valueSTO.getName() + " into " + varSto.getName());
				storeRegValueInMem(varSto, SparcInstr.REG_LOCAL1);
			}
			
			//float with float or float with int
			if ((valuetyp.isFloat() && vartyp.isFloat())||(vartyp.isFloat() && valuetyp.isInt()))
			{
				loadFloatORIntInReg(valueSTO, SparcInstr.REG_FLOAT0);
				/*
				String finalName = "zachfloat" + "_" + valueSTO.getName();
				if(valuetyp.isInt())
				{
					finalName += ".0";
					valueSTO.setType(new FloatType(valuetyp.getName(), valuetyp.getSize()));
				}
	    		
	    		writeComment("Declare " + valueSTO.getName() + " at the end of function");
	                          
	            if (!CheckExistStaticGlobal(finalName))
	            {
		            //if it does not exist in the stack yet
		            //Add Global/Static Variable to Stack 
	            	
	            	
		            addStaticGlobal(valueSTO);
		            addLocalStatic(valueSTO);
		            addLocalStaticValue(valueSTO);
	            }
	            valueSTO.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
	            
	            loadStackInReg(valueSTO, SparcInstr.REG_FLOAT0);
	            */
	            writeComment("Store " + valueSTO.getName() + " into " + varSto.getName());
				storeRegValueInMem(varSto, SparcInstr.REG_FLOAT0);
		        
			} 
			//bool with bool
			if (valuetyp.isBool() && vartyp.isBool())
			{
				writeBooleanToIntAsm(valueSTO, SparcInstr.REG_LOCAL1);
				/*
				//if it's a true const
				if(valueSTO.getName().equals("true"))
				{
					loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
				}
				//false const
				else
				{
					loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
				}*/
				writeComment("Store " + valueSTO.getName() + " into " + varSto.getName());
				storeRegValueInMem(varSto, SparcInstr.REG_LOCAL1);
			}
			if (valuetyp.isNullPointer() && (vartyp.isPointer() ||vartyp.isFunctionPointer()))
			{
				storeRegValueInMem(varSto, SparcInstr.REG_GLOBAL0);
			}
			
		}
		else	//Not const, get right hand side from stack
		{
				String reg = SparcInstr.REG_LOCAL1;
				if(valueSTO.getType().isFloat() || varSto.getType().isFloat())
				{
					reg = SparcInstr.REG_FLOAT0;
				}
				
				if (valueSTO.getType().isArray())
				{
					loadMemInReg(valueSTO, reg);
				}
				else
					loadStackInReg(valueSTO, reg);
				
				//change bit pattern from int to float
				if (valueSTO.getType().isInt() && varSto.getType().isFloat())
				{
					
					changeBitPattern(SparcInstr.FITOS_OP, reg, reg);
				}
				
				writeComment("Store " + valueSTO.getName() + " into " + varSto.getName());
				storeRegValueInMem(varSto,reg);
		}
	}
	
    private void DoAssignFP(STO varSto, STO valueSTO) {
		
    	Type t = valueSTO.getType();
    	
    	
    	if (t.getIsFuncPtr())
    	{
    		writeComment("Store function in funcptr " + valueSTO.getName() + " into funcptr " + varSto.getName());
    		loadStackInReg(valueSTO, SparcInstr.REG_LOCAL1);
    		storeRegValueInMem(varSto, SparcInstr.REG_LOCAL1);
    		
    	}
    	else
    	{
    		writeComment("Store function " + valueSTO.getName() + " into funcptr" + varSto.getName());
	    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, valueSTO.getName(), SparcInstr.REG_LOCAL1);
	    	
	    	storeRegValueInMem(varSto, SparcInstr.REG_LOCAL1);
    		
    	}
		
	}
    
	//-------------------------------------------------------------------------
    //    Struct = Struct
    //-------------------------------------------------------------------------
    private void DoAssignStruct(STO varSto, STO valueSTO) {
		
    	writeComment("STMT: Struct "+varSto.getName()+" = Struct "+valueSTO.getName());
    	int size = 0;
    	
    	if (varSto.getIsArray() )
    	{
    		loadBaseOffset(varSto, varSto.getbArray());
    		
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + varSto.getType().getName());
    	
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT0);
    	}
    	else  if (varSto.getIsStruct())
    	{
    		loadBaseOffset(varSto, varSto.getbStruct());
    		
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + varSto.getType().getName());
    	
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT0);
    	}
    	else if (varSto.getIsDeref())
    	{
    		loadBaseOffset(varSto, varSto.getbPointer());
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + varSto.getType().getName());
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT0);
    	}
    	else
    	{
	    	
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
	    				"Size of the Struct " + varSto.getType().getName());
	    	
		    writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, varSto.getOffset(), SparcInstr.REG_LOCAL0);
		    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, varSto.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    if (varSto.getIsRef())
		    {
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
		    }
		    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT0);
    	}
    	
       	if (valueSTO.getIsArray() )
    	{
    		loadBaseOffset(valueSTO, valueSTO.getbArray());
    		
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + valueSTO.getType().getName());
    	
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT1);
    	}
    	else  if (valueSTO.getIsStruct())
    	{
    		loadBaseOffset(valueSTO, valueSTO.getbStruct());
    		
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + valueSTO.getType().getName());
    	
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT1);
    	}
    	else if (valueSTO.getIsDeref())
    	{
    		loadBaseOffset(valueSTO, valueSTO.getbPointer());
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + valueSTO.getType().getName());
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL4, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT1);
    	}
    	else
    	{
	    	
	    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
	    				"Size of the Struct " + valueSTO.getType().getName());
	    	
		    writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, valueSTO.getOffset(), SparcInstr.REG_LOCAL0);
		    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, valueSTO.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
		    if (valueSTO.getIsRef())
		    {
		    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
		    }
		    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT1);
    	}
    	
    	/*
    	writeAssembly(SparcInstr.TWO_PARAM_COMM, SparcInstr.SET_OP, size+"", SparcInstr.REG_LOCAL3, 
    				"Size of the Struct " + varSto.getType().getName());
	    writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, valueSTO.getOffset(), SparcInstr.REG_LOCAL0);
	    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, valueSTO.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
	    
	    if (valueSTO.getIsRef())
	    {
	    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
	    }
	    
	    writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL3, SparcInstr.REG_OUTPUT1);
	    */
	    
    	loadLiteralInReg(new ConstSTO(varSto.getType().getSize()+""), SparcInstr.REG_OUTPUT2);
    	
    	writeCallFuncName(SparcInstr.MEM_CPY);
    	writeComment(SparcInstr.BLANK_LINE);

	}
    
	//-------------------------------------------------------------------------
    //     x = -a where a is not const
    //-------------------------------------------------------------------------
	public void DoUnarySignExpr(STO from, STO des, String u) {
		if (MyParser.isDeclType)
			return;
		
		//Store result into %fp - x
		int offset = getNextOffset();
		String op = SparcInstr.NEG_OP;
		String reg = SparcInstr.REG_LOCAL1;
		
		if (from.getType().isFloat())
		{
			op = SparcInstr.FNEGS_OP;
			reg= SparcInstr.REG_FLOAT0;
		}
		
		writeComment("Expr: "+ u + from.getAsmName());
		loadStackInReg(from, reg);
		if (u.equals("-"))
		{
				writeAssembly(SparcInstr.TWO_PARAM_COMM, op, reg, reg, 
						"make "+ from.getName() + " negative");
				
		}
		//store offset and base 
		des.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		storeRegValueInMem(des, reg);
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
   
	//-------------------------------------------------------------------------
    //     if (expr)
    //-------------------------------------------------------------------------
	public void DoIfStmt(STO sto) {
		// TODO Auto-generated method stub
		
		writeComment("if ("+sto.getName()+") is false, go to end_if/else!?");
		if (sto.isConst() && sto.getAsmName().equals(""))
		{
			if (sto.getType().isBool())
			{
				writeBooleanToIntAsm(sto, SparcInstr.REG_OUTPUT1);
			}
			else
				loadLiteralInReg(sto, SparcInstr.REG_OUTPUT1);
			
		}
		else
		{
			loadStackInReg(sto, SparcInstr.REG_OUTPUT1);
		}
		
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_OUTPUT1 );
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "else_"+endif_num);
		writeAssembly(SparcInstr.NOP_OP);		        
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        writeComment("Below is the case when if statement is true");
		
        addIfNum(endif_num);
        
        endif_num++;
	}
	
    //-------------------------------------------------------------------------
    //     Used with if(expr) {} 
    //-------------------------------------------------------------------------
	public void DoEndIf(STO sto) {
		// TODO Auto-generated method stub
		int num = getIfStack().lastElement();
		

		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_if_"+num);
		writeAssembly(SparcInstr.NOP_OP);	
		writeAssembly(SparcInstr.BLANK_LINE);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		

		decreaseIndent();
		decreaseIndent();
		writeComment("Below is the case when if statement is false");
		writeAssembly(SparcInstr.LABEL, "else_"+num);
		increaseIndent();
		increaseIndent();
		
		writeAssembly(SparcInstr.BLANK_LINE);
	}
	
    //-------------------------------------------------------------------------
    //     Used with if(expr) {} else {}
    //-------------------------------------------------------------------------
	public void DoEndIfElse(STO sto) {
		// TODO Auto-generated method stub
		int num =  getIfStack().lastElement();
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "end_if_"+num);
		increaseIndent();
		increaseIndent();
		
		getIfStack().remove(getIfStack().size()-1);
	}
	
    //-------------------------------------------------------------------------
    //     write const boolean in integer (1/0) in assembly
    //-------------------------------------------------------------------------
	public void writeBooleanToIntAsm (STO sto, String reg)
	{
		//if it's a true const
		if(sto.getName().equals("true"))
		{
			loadLiteralInReg(new ConstSTO("1"),reg);
		}
		//false const
		else
		{
			loadLiteralInReg(new ConstSTO("0"),reg);
		}
	}
	
    //-------------------------------------------------------------------------
    //     ++x or x++ or --x or x--
	//		Only for int, call FloatIncDec if is float
    //-------------------------------------------------------------------------
	public void DoIncDecOP(STO a, String o, STO result, String prepost) {
		
		if (MyParser.isDeclType)
			return;
		
		String reg = SparcInstr.REG_LOCAL1;
		String op = SparcInstr.INC_OP;
		
		writeComment("Expr: "+ prepost + " "+ o + " " +a.getName());
		
		if (a.getType().isFloat())
		{
			DoFloatIncDecOP( a,  o,  result,  prepost);
			
		}
		else if (a.getType().isInt())
		{
			int offset = getNextOffset();
			result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
			
			if (o.equals("--"))
				op = SparcInstr.DEC_OP;
			
			//put a to l1
			loadStackInReg(a, reg);
			
			//if pre inc, do inc now and store in both a and result
			//else store in result and inc. 
			//Then store in a
			if (prepost.equals("pre"))
			{				
				writeAssembly(SparcInstr.ONE_PARAM_COMM, op, reg ,
					"pre inc/dec "+ a.getName());
				storeRegValueInMem(result, reg);
				storeRegValueInMem(a, reg);
			}
			else
			{
				storeRegValueInMem(result, reg);
				writeAssembly(SparcInstr.ONE_PARAM_COMM, op, reg ,
						"post inc/dec "+ a.getName());
					
				storeRegValueInMem(a, reg);
			}
			
			writeAssembly(SparcInstr.BLANK_LINE);
		}	
		else if(prepost.equals("not"))
		{
			DoNotOP(a,  result);
		}
		else if (a.getType().isPointer())
		{
			DoPointerIncDecOP(a, o, result, prepost);
		}
		
	}
	
	private void DoPointerIncDecOP(STO a, String o, STO result, String prepost) {
		
		String reg = SparcInstr.REG_LOCAL1;
		String op = SparcInstr.ADD_OP;
		
		int offset = getNextOffset();
		int size = ((PointerType)a.getType()).getpointType().getSize();
		
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		
		if (o.equals("--"))
			op = SparcInstr.SUB_OP;
		
		//put a to l1
		loadStackInReg(a, reg);
		
		//set size into local3
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, size+"" , SparcInstr.REG_LOCAL3);
		
		//if pre inc, do inc now and store in both a and result
		//else store in result and inc. 
		//Then store in a
		if (prepost.equals("pre"))
		{				
			writeAssembly(SparcInstr.THREE_PARAM_COMM, op, reg , SparcInstr.REG_LOCAL3, reg,
				"pre inc/dec "+ a.getName());
			storeRegValueInMem(result, reg);
			storeRegValueInMem(a, reg);
		}
		else
		{
			storeRegValueInMem(result, reg);
			writeAssembly(SparcInstr.THREE_PARAM_COMM, op, reg , SparcInstr.REG_LOCAL3, reg,
					"post inc/dec "+ a.getName());
				
			storeRegValueInMem(a, reg);
		}
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	//-------------------------------------------------------------------------
    //     !boolean			e.g !true
    //-------------------------------------------------------------------------
	private void DoNotOP(STO a, STO result) {
		if (MyParser.isDeclType)
			return;
    	//Const boolean : do not care, MyParser takes care of that
    	//check if result is const and bool
    	if (!result.isConst() && a.getType().isBool())
    	{
    		loadStackInReg(a, SparcInstr.REG_LOCAL5);
			writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL5 );
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "not_false_"+bool_num);
			writeAssembly(SparcInstr.NOP_OP);		        
            writeAssembly(SparcInstr.BLANK_LINE);
            
            //True
            loadLiteralInReg(new ConstSTO("0"),SparcInstr.REG_LOCAL1);
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "not_end_"+bool_num);
			writeAssembly(SparcInstr.NOP_OP);		            
            writeAssembly(SparcInstr.BLANK_LINE);
            
            //False
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "not_false_"+bool_num);
			increaseIndent();
			increaseIndent();
			loadLiteralInReg(new ConstSTO("1"),SparcInstr.REG_LOCAL1);
			
			//End
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "not_end_"+bool_num);
			increaseIndent();
			increaseIndent();
			
			//inc bool_num
			bool_num++;
			
			//store result into stack
			int offset = getNextOffset();
			result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
			storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
    	}
		
	}
    
	//-------------------------------------------------------------------------
    //     float pre/post inc/dec
    //-------------------------------------------------------------------------
	private void DoFloatIncDecOP(STO a, String o, STO result, String prepost) {
		if (MyParser.isDeclType)
			return;
		
		String reg = SparcInstr.REG_FLOAT0;
		String op = SparcInstr.FADDS_OP;
		
		int offset = getNextOffset();
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		
		if (o.equals("--"))
			op = SparcInstr.FSUBS_OP;
		
		//put a to l1
		loadStackInReg(a, reg);
		
    	writeComment("Load 1.0 into %f1");
    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.FloatINCDEC, SparcInstr.REG_LOCAL0);
    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_FLOAT1);
    	writeAssembly(SparcInstr.BLANK_LINE);
		
		//if pre inc, do inc now and store in both a and result
		//else store in result and inc. 
		//Then store in a
		if (prepost.equals("pre"))
		{				
			writeAssembly(SparcInstr.THREE_PARAM_COMM, op, reg , SparcInstr.REG_FLOAT1, reg, 
				"pre inc/dec "+ a.getName());
			storeRegValueInMem(result, reg);
			storeRegValueInMem(a, reg);
		}
		else
		{
			storeRegValueInMem(result, reg);
			writeAssembly(SparcInstr.THREE_PARAM_COMM, op, reg , SparcInstr.REG_FLOAT1, reg, 
					"post inc/dec "+ a.getName());
				
			storeRegValueInMem(a, reg);
		}
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	
	//-------------------------------------------------------------------------
    //     call funcname
	//	   nop
    //-------------------------------------------------------------------------
	public void DoFuncCall(STO sto, Vector<STO> args) {
		if (MyParser.isDeclType)
			return;
		
		writeComment("STMT: Do Function Call : " + sto.getName());

		Vector<STO> params;
		if (sto.getType().getIsFuncPtr())
		{
			params = ((FunctionPointerType)sto.getType()).getParams();
			loadStackInReg(sto, SparcInstr.REG_LOCAL1);
			CheckNullPointerFunc(sto);
		}
		else
		{
			FuncSTO funcsto = (FuncSTO)sto;
			params = funcsto.getParam();
		}

			
			int floatI = 0;
			int intI = 0;
			
			//Add arg to output
			for (int i = 0; i< args.size(); i++)
			{
				STO arg = args.get(i);
				boolean isRef = params.get(i).getIsRef();
				
				//literal
				if (arg.isConst() && !arg.getIsAddressable())
				{
					if (arg.getType().isInt() || arg.getType().isBool())
					{
						if (params.get(i).getType().isFloat())
						{
							loadFloatORIntInReg(arg, "%f" + floatI++);
						}
						else
						{
							if (arg.getType().isInt())
								loadLiteralInReg(arg, "%o" + intI++);
							if (arg.getType().isBool())
								writeBooleanToIntAsm(arg, "%o" + intI++);							
						}
					}
					else if(arg.getType().isFloat())
					{
						loadFloatORIntInReg(arg, "%f" + floatI++);
					}
				}
				else
				{
					if (isRef)
					{
						if (!arg.getIsArray() && !arg.getIsStruct() && !arg.getIsDeref())
						{
							if (arg.getIsRef())
							{
					    		writeComment("store ref of "+arg.getName()+ " into reference output reg");
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, arg.getOffset(), SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, arg.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
						    	MoveRegToReg(SparcInstr.REG_LOCAL0, "%o" + intI++);
							}
							else
							{
								writeComment("Store reference "+ arg.getName() +" into "+"%o" + intI);
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, arg.getOffset(), SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, arg.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
								MoveRegToReg(SparcInstr.REG_LOCAL0, "%o" + intI++);
							}
						}
						else if (arg.getIsArray())
						{	
							loadBaseOffset(arg, arg.getbArray());
							MoveRegToReg(SparcInstr.REG_LOCAL4, "%o" + intI++);
						}
						else if (arg.getIsStruct())
						{
								loadBaseOffset(arg, arg.getbStruct());
								MoveRegToReg(SparcInstr.REG_LOCAL4, "%o" + intI++);
						}
						else if (arg.getIsDeref())
						{
								loadBaseOffset(arg, arg.getbStruct());
								MoveRegToReg(SparcInstr.REG_LOCAL4, "%o" + intI++);
						}
						
						writeAssembly(SparcInstr.BLANK_LINE);
					}
					else
					{
						if (arg.getType().isInt() || arg.getType().isBool())
						{
							if (params.get(i).getType().isFloat())
							{
								if (arg.getIsRef())
								{
						    		writeComment("store ref of "+arg.getName()+ " into reference output reg");
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, arg.getOffset(), SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, arg.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", "%f" + floatI);
							    	writeComment("Turn int "+arg.getName()+" to float");
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.FITOS_OP,  "%f" + floatI,  "%f" + floatI);
									floatI ++;
								}
								else
								{
									writeComment("Turn int "+arg.getName()+" to float");
									loadStackInReg(arg, "%f" + floatI);
									writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.FITOS_OP,  "%f" + floatI,  "%f" + floatI);
									floatI ++;
								}
							}
							else
							{
								if (arg.getIsRef())
								{
						    		writeComment("store ref of "+arg.getName()+ " into reference output reg");
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, arg.getOffset(), SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, arg.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
							    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", "%o" + intI++);
								}
								else
								{
									loadStackInReg(arg, "%o" + intI++);
								}
							}
						}
						else if(arg.getType().isFloat())
						{
							if (arg.getIsRef())
							{
					    		writeComment("store ref of "+arg.getName()+ " into reference output reg");
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, arg.getOffset(), SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, arg.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
						    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", "%f" + floatI++);
							}
							else
							{
								loadStackInReg(arg, "%f" + floatI++);
							}
						}
						else if (arg.getType().isPointer())
						{
							loadBaseOffset(arg, arg);
							MoveRegToReg(SparcInstr.REG_LOCAL4, "%o" + intI++);
						}
						else if (arg.getType().isArray())
						{
							loadMemInReg(arg, "%o" + intI++);
						}
					}
				}
				
				
			}
			
			String name = sto.getName();
			
			if (sto.getType().getIsFuncPtr())
			{
				loadStackInReg(sto, SparcInstr.REG_LOCAL1);
				name = SparcInstr.REG_LOCAL1;
			}
			
			writeCallFuncName(name);
			writeAssembly(SparcInstr.BLANK_LINE);
		
		
	}
	
	private void CheckNullPointerFunc(STO sto) {
		
		writeComment("Check for NULL pointer");
		
		
		MoveRegToReg(SparcInstr.REG_LOCAL1, SparcInstr.REG_LOCAL5);
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL5);
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BNE_OP, "is_pointer_"+ deref_num );
		writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
		
		writeAssembly(SparcInstr.BLANK_LINE);
		writeComment("NULL Pointer : true");
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.STRNULLPOINTER, SparcInstr.REG_OUTPUT0);
		writeCallFuncName(SparcInstr.PRINTF);
		
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "1", SparcInstr.REG_OUTPUT0);
		writeCallFuncName(SparcInstr.EXIT);
		
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "is_pointer_"+ deref_num );
		increaseIndent();
		increaseIndent();
		
		deref_num++;
		
	}
	//Done????????
	//-------------------------------------------------------------------------
    //     Store return value into %i0 or %f0
    //-------------------------------------------------------------------------
	public void storeReturnFromFunction(STO result, boolean isRef) {
		if (MyParser.isDeclType || result.getType().isVoid())
			return;
		
		writeComment("Store return value to stack from function " + result.getName());
		
		String reg = SparcInstr.REG_OUTPUT0;
		
		if (result.getType().isFloat())
		{
			reg= SparcInstr.REG_FLOAT0;
		}
	
		int offset = getNextOffset();
		result.store(SparcInstr.REG_FRAME, offset+"", "local_yiu");
		
		storeRegValueInMem(result, reg);
		
		if (isRef)
		{
			result.setIsRef(true);
		}
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	//-------------------------------------------------------------------------
	//     no output
    //     ret
	//	   restore
    //-------------------------------------------------------------------------
	public void DoReturnNoExpr() {
		
		writeComment("Return with no expr ");
		writeReturn();
	}
	
	//-------------------------------------------------------------------------
	//	   set output to %i0 or %f0
    //     ret
	//	   restore
    //-------------------------------------------------------------------------
	public void DoReturnWithExpr(STO ret, boolean isRef, boolean isFloat) {
		
		writeComment("Return " + ret.getName());
		
		Type typ = ret.getType();
		if (isRef)
		{
			/*
	    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, ret.getOffset(), SparcInstr.REG_LOCAL0);
	    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, ret.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL1);
			if (ret.getIsRef())
			{
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "["+SparcInstr.REG_LOCAL1+"]", SparcInstr.REG_LOCAL1);
			}
			MoveRegToReg(SparcInstr.REG_LOCAL1, SparcInstr.REG_INPUT0);
			*/
			if (!ret.getIsArray() && !ret.getIsStruct() && !ret.getIsDeref())
			{
				if (ret.getIsRef())
				{
		    		writeComment("store ref of "+ret.getName()+ " into %i0");
			    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, ret.getOffset(), SparcInstr.REG_LOCAL0);
			    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, ret.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
			    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL0 + "]", SparcInstr.REG_LOCAL0);
			    	MoveRegToReg(SparcInstr.REG_LOCAL0, SparcInstr.REG_INPUT0);
				}
				else
				{
					writeComment("Store reference "+ ret.getName() +" into %i0");
			    	writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, ret.getOffset(), SparcInstr.REG_LOCAL0);
			    	writeAssembly(SparcInstr.THREE_PARAM, SparcInstr.ADD_OP, ret.getBase(), SparcInstr.REG_LOCAL0, SparcInstr.REG_LOCAL0);
			    	MoveRegToReg(SparcInstr.REG_LOCAL0, SparcInstr.REG_INPUT0);
				}
			}
			else if (ret.getIsArray())
			{	
				loadBaseOffset(ret, ret.getbArray());
				MoveRegToReg(SparcInstr.REG_LOCAL4, SparcInstr.REG_INPUT0);
			}
			else if (ret.getIsStruct())
			{
					loadBaseOffset(ret, ret.getbStruct());
					MoveRegToReg(SparcInstr.REG_LOCAL4, SparcInstr.REG_INPUT0);
			}
			else if (ret.getIsDeref())
			{
					loadBaseOffset(ret, ret.getbStruct());
					MoveRegToReg(SparcInstr.REG_LOCAL4, SparcInstr.REG_INPUT0);
			}
			
			writeAssembly(SparcInstr.BLANK_LINE);
			
		}
		else
		{
			if (typ.isInt() || typ.isBool())
			{
				if (ret.isConst() && !ret.getIsAddressable())
				{
					if (typ.isInt())
					{
						if (isFloat)
						{
							writeComment("Turn const int "+ret.getName()+" to float");
							int offset = getNextOffset();
							
							loadLiteralInReg(ret, SparcInstr.REG_LOCAL1);
							writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP, SparcInstr.REG_LOCAL1, "[%fp"+offset+"]");
							writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[%fp"+offset+"]", SparcInstr.REG_FLOAT0);
							writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.FITOS_OP,  SparcInstr.REG_FLOAT0,  SparcInstr.REG_FLOAT0);
						}
						else
						{
							loadLiteralInReg(ret, SparcInstr.REG_INPUT0);
						}
					}
					if (typ.isBool())
					{
						writeBooleanToIntAsm(ret, SparcInstr.REG_INPUT0);
					}
				}
				else
				{
					if (isFloat)
					{
						writeComment("Turn int "+ret.getName()+" to float");
						loadStackInReg(ret,SparcInstr.REG_FLOAT0);
						writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.FITOS_OP,  SparcInstr.REG_FLOAT0,  SparcInstr.REG_FLOAT0);
					}
					else
					{
						loadStackInReg(ret,SparcInstr.REG_INPUT0);
					}
				}
			}
			else if (typ.isFloat())
			{
				if (ret.isConst() && !ret.getIsAddressable())
				{
					String finalName = "zachfloat" + "_";
		        	if (ret.getName().charAt(0) == '-')
		        		finalName+=  "n"+ret.getName().substring(1);
		        	else
		        		finalName+=  ret.getName();
		    		
		    		writeComment("Declare " + ret.getName() + " at the end of function");
		                          
		            if (!CheckExistStaticGlobal(finalName))
		            {
			            //if it does not exist in the stack yet
			            //Add Global/Static Variable to Stack 
		            	
			            addStaticGlobal(ret);
			            addLocalStatic(ret);
			            addLocalStaticValue(ret);
		            }
		            ret.store(SparcInstr.REG_GLOBAL0, finalName, finalName);
		            
		            loadStackInReg(ret, SparcInstr.REG_FLOAT0);
		            writeComment("Store " + ret.getName() + " into " + ret.getName());
		            
				}

				loadStackInReg(ret,SparcInstr.REG_FLOAT0);
			}
			else if (typ.isPointer() )
			{
				loadStackInReg(ret,SparcInstr.REG_INPUT0);
			}
			else if (typ.isArray())
			{
				loadMemInReg(ret, SparcInstr.REG_INPUT0);
			}
		}
		
		writeReturn();
	}
	
	//-------------------------------------------------------------------------
    //     ret
	//	   restore
    //-------------------------------------------------------------------------
	public void writeReturn()
	{
        writeAssembly(SparcInstr.NO_PARAM, SparcInstr.RET_OP);          // ret
        writeAssembly(SparcInstr.NO_PARAM, SparcInstr.RESTORE_OP);      // restore
        writeAssembly(SparcInstr.BLANK_LINE);
	}
	
	//-------------------------------------------------------------------------
    //     call exit
    //     nop
    //-------------------------------------------------------------------------
	public void DoExit(STO a) {
		
		writeComment("System call :  Exit ("+ a.getName()+")");
		
		if (a.isConst())
		{
			loadLiteralInReg(a, SparcInstr.REG_OUTPUT0);
		}
		else
		{
			loadStackInReg(a,SparcInstr.REG_OUTPUT0);
		}
		
		writeCallFuncName(SparcInstr.EXIT);
		writeAssembly(SparcInstr.BLANK_LINE);
	}
	
	//-------------------------------------------------------------------------
    //     cin<<var;
    //-------------------------------------------------------------------------
	public void DoCin(STO sto)
	{
		if (sto.getType().isInt())
		{
			writeComment("STMT: cin >> "+sto.getName() + " (int)");
			writeCallFuncName(SparcInstr.INPUTINT);
			writeAssembly(SparcInstr.BLANK_LINE);
			
			writeComment("store cin result to "+ sto.getName());
			storeRegValueInMem(sto, SparcInstr.REG_OUTPUT0);
		}
		else if (sto.getType().isFloat())
		{
			writeComment("STMT: cin >> "+sto.getName() + " (float)");
			writeCallFuncName(SparcInstr.INPUTFLOAT);
			writeAssembly(SparcInstr.BLANK_LINE);
			
			writeComment("store cin result to "+ sto.getName());
			storeRegValueInMem(sto, SparcInstr.REG_FLOAT0);
		}
		
		writeAssembly(SparcInstr.BLANK_LINE);
	}
	
	//-------------------------------------------------------------------------
    //     while loop
    //-------------------------------------------------------------------------
	public void DoWhile(STO sto)
	{
		writeComment("STMT: while ("+sto.getName()+") is true");
		
		
		int num = getWhileStack().lastElement();
		
		if (sto.isConst() && sto.getAsmName().equals(""))
		{
			if (sto.getType().isBool())
			{
				writeBooleanToIntAsm(sto, SparcInstr.REG_OUTPUT1);
			}
			else
				loadLiteralInReg(sto, SparcInstr.REG_OUTPUT1);
			
		}
		else
		{
			loadStackInReg(sto, SparcInstr.REG_OUTPUT1);
		}
		
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_OUTPUT1 );
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "end_while_"+num);
		writeAssembly(SparcInstr.NOP_OP);		        
        writeAssembly(SparcInstr.BLANK_LINE);
        writeAssembly(SparcInstr.BLANK_LINE);
        writeComment("Below is the case when while loop is true");
		
	}
	
	
	//-------------------------------------------------------------------------
    //     end while (print end label)
    //-------------------------------------------------------------------------
	public void DoEndWhile(STO sto)
	{
		int num = getWhileStack().lastElement();
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "while_"+num);
		writeAssembly(SparcInstr.NOP_OP);	
		writeAssembly(SparcInstr.BLANK_LINE);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		decreaseIndent();
		decreaseIndent();
		writeComment("The end of while Loop");
		writeAssembly(SparcInstr.LABEL, "end_while_"+num);
		increaseIndent();
		increaseIndent();
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
		getWhileStack().remove(getWhileStack().size()-1);
	}
	
	//-------------------------------------------------------------------------
    //     end while (print while label)
    //-------------------------------------------------------------------------
	public void DoStartWhile()
	{
		
		decreaseIndent();
		decreaseIndent();
		writeComment("WHILE LABEL STARTS HERE");
		writeAssembly(SparcInstr.LABEL, "while_"+while_num);
		increaseIndent();
		increaseIndent();
		
		addWhileNum(while_num);
		while_num ++;
	}
	
	//-------------------------------------------------------------------------
    //     ba to while
    //-------------------------------------------------------------------------
	public void DoContinue()
	{
		int num = getWhileStack().lastElement();
		
		writeComment("STMT: Continue to while_" +num);
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "while_"+num);
		writeAssembly(SparcInstr.NOP_OP);
		writeAssembly(SparcInstr.BLANK_LINE);
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	//-------------------------------------------------------------------------
    //     break to the end of while
    //-------------------------------------------------------------------------
	public void DoBreak()
	{
		int num = getWhileStack().lastElement();
		
		writeComment("STMT: Break to end_while_" +num);
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_while_"+num);
		writeAssembly(SparcInstr.NOP_OP);
		writeAssembly(SparcInstr.BLANK_LINE);
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	
	public void DoAddressOf(STO sto, STO result) {
		if (MyParser.isDeclType)
			return;
		
		writeComment("EXPR: Address of " + sto.getName());
		
		
		loadMemInReg(sto, SparcInstr.REG_LOCAL1);
		
		int offset = getNextOffset();
		
		result.store(SparcInstr.REG_FRAME, ""+offset, sto.getName());
		
		writeComment("Store address into tmp");
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		
	}
	
	public void DoStar(STO sto, STO result)
	{
		if (MyParser.isDeclType)
			return;
		
		writeComment("EXPR: Star of " + sto.getName());
		writeComment("Load Variable address");
		loadStackInReg(sto, SparcInstr.REG_LOCAL1);
		
		CheckNullPointerFunc(sto);
		
		writeComment("Dereference the address");
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "["+SparcInstr.REG_LOCAL1+"]", SparcInstr.REG_LOCAL1);
		
		int offset = getNextOffset();
		
		
		result.store(SparcInstr.REG_FRAME, ""+offset, sto.getName());
		
		writeComment("Store address into tmp");
		storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
		
		writeAssembly(SparcInstr.BLANK_LINE);
		
	}
	
	public void DoNew(STO sto) {
		writeComment("STMT: NEW for " + sto.getName());
		writeComment("void *calloc (size_t numofElement, size_t size");
		
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "1", SparcInstr.REG_OUTPUT0);
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "" + ((PointerType)sto.getType()).getpointType().getSize(), SparcInstr.REG_OUTPUT1);
		
		writeCallFuncName(SparcInstr.CALLOC);
		writeAssembly(SparcInstr.BLANK_LINE);
		
		storeRegValueInMem(sto, SparcInstr.REG_OUTPUT0);
		
	}
	
	public void DoDelete(STO sto) {
		writeComment("STMT: Delete for " + sto.getName());
		writeComment("void free (void *ptr)");
		
		loadMemInReg(sto, SparcInstr.REG_OUTPUT0);
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "["+SparcInstr.REG_OUTPUT0+"]", SparcInstr.REG_OUTPUT0);
		
		writeComment("Check for NULL pointer");
		MoveRegToReg(SparcInstr.REG_OUTPUT0, SparcInstr.REG_LOCAL5);
		
		//instr	reg1, reg2, reg1
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP, SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL5);
		
		writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BNE_OP, "is_pointer_"+ deref_num );
		writeAssembly(SparcInstr.NO_PARAM, SparcInstr.NOP_OP);
		
		writeAssembly(SparcInstr.BLANK_LINE);
		writeComment("NULL Pointer : true");
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.STRNULLPOINTER, SparcInstr.REG_OUTPUT0);
		writeCallFuncName(SparcInstr.PRINTF);
		
		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "1", SparcInstr.REG_OUTPUT0);
		writeCallFuncName(SparcInstr.EXIT);
		
		decreaseIndent();
		decreaseIndent();
		writeAssembly(SparcInstr.LABEL, "is_pointer_"+ deref_num );
		increaseIndent();
		increaseIndent();
		
		deref_num++;
		
		writeCallFuncName(SparcInstr.FREE);
		//writeAssembly(SparcInstr.BLANK_LINE);
		
		writeComment("Set " + sto.getName() + " points to NULL(0)");
		storeRegValueInMem(sto, SparcInstr.REG_GLOBAL0);
		
	}
	public void doTypecast(STO sto, Type t, STO result) {
		
		writeComment("Casting from "+sto.getType().getName()+" to "+t.getName());
		int offset = getNextOffset();
		result.store(SparcInstr.REG_FRAME, offset+"", sto.getName());
		
		if (t.isNullPointer())
		{
			storeRegValueInMem(result, SparcInstr.REG_GLOBAL0);
		}
		
		Type baseT = t;
		Type typ  = sto.getType();
		
		while (baseT.isPointer())
		{
			baseT = ((PointerType)baseT).getpointType();
		}
		
		if (typ.isInt())
		{
			if (t.isBool())
			{
				
				loadStackInReg(sto, SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL1 );
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "false_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		        
	            writeAssembly(SparcInstr.BLANK_LINE);
	            
	            loadLiteralInReg(new ConstSTO("1"), SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		            
	            writeAssembly(SparcInstr.BLANK_LINE);
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "false_"+bool_num);
				increaseIndent();
				increaseIndent();
				
				storeRegValueInMem(result, SparcInstr.REG_GLOBAL0);
				
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
				increaseIndent();
				increaseIndent();
				bool_num++;
				
			}
			if (t.isPointer())
			{
				writeComment("Casting int into pointer : Do Nothing");
				loadStackInReg(sto, SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			}
			if (t.isFloat())
			{
				loadStackInReg(sto, SparcInstr.REG_FLOAT0);
				changeBitPattern(SparcInstr.FITOS_OP, SparcInstr.REG_FLOAT0, SparcInstr.REG_FLOAT0);
				storeRegValueInMem(result, SparcInstr.REG_FLOAT0);
			}
			if (t.isInt())
			{
				writeComment("Casting int into int : Do Nothing");
				loadStackInReg(sto, SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			}
		}
		if (typ.isBool())
		{
			if (!t.isFloat())
			{
				loadStackInReg(sto, SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			}
			if (t.isFloat())
			{
				loadStackInReg(sto, SparcInstr.REG_FLOAT0);
				changeBitPattern(SparcInstr.FITOS_OP, SparcInstr.REG_FLOAT0, SparcInstr.REG_FLOAT0);
				storeRegValueInMem(result, SparcInstr.REG_FLOAT0);

			}
			
		}
		if (typ.isFloat())
		{
			if (t.isInt() || t.isPointer())
			{

				loadStackInReg(sto, SparcInstr.REG_FLOAT0);
				changeBitPattern("fstoi", SparcInstr.REG_FLOAT0, SparcInstr.REG_FLOAT0);
				
				int off = getNextOffset();
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP,  SparcInstr.REG_FLOAT0, "[%fp"+off+"]");
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[%fp"+off+"]", SparcInstr.REG_LOCAL1);
				
				
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);

			}
			
			if (t.isBool())
			{

				loadStackInReg(sto, SparcInstr.REG_FLOAT0);
				changeBitPattern("fstoi", SparcInstr.REG_FLOAT0, SparcInstr.REG_FLOAT0);
				
				int off = getNextOffset();
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.STORE_OP,  SparcInstr.REG_FLOAT0, "[%fp"+off+"]");
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[%fp"+off+"]", SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL1 );
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "false_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		        
	            writeAssembly(SparcInstr.BLANK_LINE);
	            
	            loadLiteralInReg(new ConstSTO("1"), SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		            
	            writeAssembly(SparcInstr.BLANK_LINE);
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "false_"+bool_num);
				increaseIndent();
				increaseIndent();
				
				storeRegValueInMem(result, SparcInstr.REG_GLOBAL0);
				
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
				increaseIndent();
				increaseIndent();
				bool_num++;

			}
			if (t.isFloat())
			{
				loadStackInReg(sto, SparcInstr.REG_FLOAT0);
				storeRegValueInMem(result, SparcInstr.REG_FLOAT0);
			}
			
		}
		if (typ.isPointer())
		{
			if (t.isInt())
			{
				writeComment("Casting int into pointer : Do Nothing");
				loadMemInReg(sto, SparcInstr.REG_LOCAL1);
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL1 + "]", SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			}
			if (t.isBool())
			{
				loadMemInReg(sto, SparcInstr.REG_LOCAL1);
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL1 + "]", SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_GLOBAL0, SparcInstr.REG_LOCAL1 );
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BE_OP, "false_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		        
	            writeAssembly(SparcInstr.BLANK_LINE);
	            
	            loadLiteralInReg(new ConstSTO("1"), SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
				
				writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BA_OP, "end_"+bool_num);
				writeAssembly(SparcInstr.NOP_OP);		            
	            writeAssembly(SparcInstr.BLANK_LINE);
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "false_"+bool_num);
				increaseIndent();
				increaseIndent();
				
				storeRegValueInMem(result, SparcInstr.REG_GLOBAL0);
				
				decreaseIndent();
				decreaseIndent();
				writeAssembly(SparcInstr.LABEL, "end_"+bool_num);
				increaseIndent();
				increaseIndent();
				bool_num++;
			}
			if (t.isFloat())
			{
				loadMemInReg(sto, SparcInstr.REG_LOCAL1);
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL1 + "]", SparcInstr.REG_FLOAT0);
				
				changeBitPattern(SparcInstr.FITOS_OP, SparcInstr.REG_FLOAT0, SparcInstr.REG_FLOAT0);
				storeRegValueInMem(result, SparcInstr.REG_FLOAT0);
			}
			if (t.isPointer())
			{
				loadMemInReg(sto, SparcInstr.REG_LOCAL1);
				writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.LOAD_OP, "[" + SparcInstr.REG_LOCAL1 + "]", SparcInstr.REG_LOCAL1);
				storeRegValueInMem(result, SparcInstr.REG_LOCAL1);
			}
		}

	}
	
	
	public void DoRunTimeArrayChecking(STO sto, STO result) {
		writeComment("Run time Array Index Checking");
		
		if (sto.getType().isArray())
		{
			ArrayType t = ((ArrayType)(sto.getType()));
			int max = t.getNumOfElement();
			STO index = result.getnIndex();
			
			loadLiteralInReg(new ConstSTO(""+max), SparcInstr.REG_LOCAL5);
			
			//index is not const
			loadStackInReg(index, SparcInstr.REG_LOCAL6);
			
			//upperbound check
			writeComment("Upper Bound check");
			
			writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_LOCAL6, SparcInstr.REG_LOCAL5 );
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BL_OP, "lower_bound_"+array_bound);
			writeAssembly(SparcInstr.NOP_OP);		        
            writeAssembly(SparcInstr.BLANK_LINE);
            
            //cout error 
            //exit
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.STROFB, SparcInstr.REG_OUTPUT0);
    		MoveRegToReg(SparcInstr.REG_LOCAL6, SparcInstr.REG_OUTPUT1);
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, max+"", SparcInstr.REG_OUTPUT2);
    		writeCallFuncName(SparcInstr.PRINTF);
    		
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "1", SparcInstr.REG_OUTPUT0);
    		writeCallFuncName(SparcInstr.EXIT);
            	            
            writeAssembly(SparcInstr.BLANK_LINE);
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "lower_bound_"+array_bound);
			increaseIndent();
			increaseIndent();
			
			writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.CMP_OP,SparcInstr.REG_LOCAL6, SparcInstr.REG_GLOBAL0 );
			writeAssembly(SparcInstr.ONE_PARAM, SparcInstr.BGE_OP, "end_array_check_"+array_bound);
			writeAssembly(SparcInstr.NOP_OP);		        
            writeAssembly(SparcInstr.BLANK_LINE);
            
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, SparcInstr.STROFB, SparcInstr.REG_OUTPUT0);
    		MoveRegToReg(SparcInstr.REG_LOCAL6, SparcInstr.REG_OUTPUT1);
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, max+"", SparcInstr.REG_OUTPUT2);
    		writeCallFuncName(SparcInstr.PRINTF);
    		
    		writeAssembly(SparcInstr.TWO_PARAM, SparcInstr.SET_OP, "1", SparcInstr.REG_OUTPUT0);
    		writeCallFuncName(SparcInstr.EXIT);
            
            //cout error
            //exit
			
			decreaseIndent();
			decreaseIndent();
			writeAssembly(SparcInstr.LABEL, "end_array_check_"+array_bound);
			increaseIndent();
			increaseIndent();
			
			array_bound++;
		}
		
	}
}