

//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

abstract class STO
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	STO (String strName)
	{
		this(strName, null);
	}

	public 
	STO (String strName, Type typ)
	{
		setName(strName);
		setType(typ);
		setIsAddressable(false);
		setIsModifiable(false);
	}
	
	


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String
	getName ()
	{
		return m_strName;
	}

	private void
	setName (String str)
	{
		m_strName = str;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Type
	getType ()
	{
		return	m_type;
	}

	protected void
	setType (Type type)
	{
		m_type = type;
	}


	//----------------------------------------------------------------
	// Addressable refers to if the object has an address. Variables
	// and declared constants have an address, whereas results from 
	// expression like (x + y) and literal constants like 77 do not 
	// have an address.
	//----------------------------------------------------------------
	public boolean
	getIsAddressable ()
	{
		return	m_isAddressable;
	}

	protected void
	setIsAddressable (boolean addressable)
	{
		m_isAddressable = addressable;
	}

	//----------------------------------------------------------------
	// You shouldn't need to use these two routines directly
	//----------------------------------------------------------------
	private boolean
	getIsModifiable ()
	{
		return	m_isModifiable;
	}

	protected void
	setIsModifiable (boolean modifiable)
	{
		m_isModifiable = modifiable;
	}


	//----------------------------------------------------------------
	// A modifiable L-value is an object that is both addressable and
	// modifiable. Objects like constants are not modifiable, so they 
	// are not modifiable L-values.
	//----------------------------------------------------------------
	public boolean
	isModLValue ()
	{
		return	getIsModifiable() && getIsAddressable();
	}

	protected void
	setIsModLValue (boolean m)
	{
		setIsModifiable(m);
		setIsAddressable(m);
	}


	//----------------------------------------------------------------
	//	It will be helpful to ask a STO what specific STO it is.
	//	The Java operator instanceof will do this, but these methods 
	//	will allow more flexibility (ErrorSTO is an example of the
	//	flexibility needed).
	//----------------------------------------------------------------
	public boolean	isVar () 	{ return false; }
	public boolean	isConst ()	{ return false; }
	public boolean	isExpr ()	{ return false; }
	public boolean	isFunc () 	{ return false; }
	public boolean	isTypedef () 	{ return false; }
	public boolean	isError () 	{ return false; }
	public boolean  isEmpty ()  { return false; }


	//----------------------------------------------------------------
	// 
	//----------------------------------------------------------------
	private String  	m_strName;
	private Type		m_type;
	private boolean		m_isAddressable;
	private boolean		m_isModifiable;
	private String		m_base="";
	private String 		m_offset="";
	private boolean isRef = false;
	private boolean isGlobal = false;
	private String	asm_name="";
	private boolean isString = false;
	private int		if_num = 0;
	private int 	while_num =0;
	private STO		nIndex;
	private boolean isArray = false;
	private STO		bArray;
	private boolean isStruct = false;
	private STO		bStruct;
	public int		num =0;
	private boolean isDeref = false;
	private STO		bPointer;
	private boolean isStatic = false;
	private String 	last_offset = "";
	
	public void setLastOffset(String b) {last_offset = b;}
	public String getLastOffset(){ return last_offset; }
	
	public void setIsStatic(boolean b) {isStatic = b;}
	public boolean getIsStatic(){ return isStatic; }
	
	public void setbPointer(STO n) { bPointer = n; }
	public STO getbPointer() { return bPointer; }
	
	public void setIsDeref(boolean b) {isDeref = b;}
	public boolean getIsDeref(){ return isDeref; }
	
	public void setbStruct(STO n) { bStruct = n; }
	public STO getbStruct() { return bStruct; }
	
	public void setIsStruct(boolean b) {isStruct = b;}
	public boolean getIsStruct(){ return isStruct; }
	
	public void setnIndex(STO n) { nIndex = n; }
	public STO getnIndex() { return nIndex; }
	
	public void setbArray(STO n) { bArray = n; }
	public STO getbArray() { return bArray; }
	
	public void setIsArray(boolean b) {isArray = b;}
	public boolean getIsArray(){ return isArray; }
	
	// Reference
	public void setIsRef(boolean t)
	{
		isRef = t;
	}
	
	public boolean getIsRef()
	{
		return isRef;
	}
	
	
	
	public void setIsGlobal(boolean t)
	{
		isGlobal = t;
	}
	
	public boolean getIsGlobal()
	{
		return isGlobal;
	}
	
	
	
	public void setBase(String base)
	{
		m_base = base;
	}
	
	public String getBase()
	{
		return m_base;
	}
	
	
	public void setOffset(String offset)
	{
		m_offset = offset;
	}
	
	public String getOffset()
	{
		return m_offset;
	}


	public void store(String base, String offset) {
		setBase(base);
		setOffset(offset);
	}
	
	public void store(String base, String offset, String s) {
		setBase(base);
		setOffset(offset);
		setAsmName(s);
	}
	
	public void setAsmName(String s) { asm_name = s; }
	public String getAsmName() { return asm_name; }
	
	public void setIsString(boolean s)
	{
		isString = s;
	}
	
	public boolean getIsString()
	{
		return isString;
	}
	
	public int getIfNum() {return if_num;}
	public void setIfNum(int i) { if_num = i; }
	
	public int getWhileNum() {return while_num;}
	public void setWhileNum(int i) { while_num = i; }
}
