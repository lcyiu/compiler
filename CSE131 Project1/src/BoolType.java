
public class BoolType extends BasicType{

	public BoolType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isBool()		{ return true; }
	
	public boolean isAssignable (Type t)
	{
		return t.isBool();
	}
	
	public boolean isEquivalent (Type t)
	{
		return t.isBool();
	}


}
