//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class ConstSTO extends STO
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	ConstSTO (String strName)
	{
		super (strName);
		m_value = null; // fix this
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
	}

	public 
	ConstSTO (String strName, Type typ)
	{
		super (strName, typ);
		if (typ != null)
		{
			if (typ.isBool())
			{
				if (strName.equals("true"))
					setValue(1);
				else
					setValue(0);
			}
			if (typ.isNumeric())
			{
				m_value = (double) Double.parseDouble(strName);
			}
				
		}
		else
			m_value = null; // fix this
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
	}
	
	public 
	ConstSTO (String strName, boolean isstring)
	{
		super (strName);
		
		this.setType(new IntType("string", 4));
		m_value = 0.0; // fix this
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
		
		setIsString(isstring);
	}
	
	

	public 
	ConstSTO(String strName, Type typ, Double value) 
	{
		// TODO Auto-generated constructor stub
		super (strName, typ);
		if ((typ.isBool() || typ.isNumeric() ) || value != null)
		{
			m_value = value;
		}
		else
			m_value = null;
	}
	
	public 
	ConstSTO(String strName, Type typ, Double value, boolean b) 
	{
		// TODO Auto-generated constructor stub
		super (strName, typ);
		if (typ.isBool() || typ.isNumeric())
		{
			m_value = value;
		}
		else
			m_value = null;
		
		setIsAddressable(b);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	isConst () 
	{
		return true;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	private void
	setValue (double val) 
	{
		m_value = new Double(val);
	}

	public Double
	getValue () 
	{
		return m_value;
	}

	public int
	getIntValue () 
	{
		return m_value.intValue();
	}

	public float
	getFloatValue () 
	{
		return m_value.floatValue();
	}

	public boolean
	getBoolValue () 
	{
		return (m_value.intValue() != 0);
	}


//----------------------------------------------------------------
//	Constants have a value, so you should store them here.
//	Note: We suggest using Java's Double class, which can hold
//	floats and ints. You can then do .floatValue() or 
//	.intValue() to get the corresponding value based on the
//	type. Booleans/Ptrs can easily be handled by ints.
//	Feel free to change this if you don't like it!
//----------------------------------------------------------------
        private Double		m_value;
}
