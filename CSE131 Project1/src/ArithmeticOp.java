public class ArithmeticOp extends BinaryOp{
	public ArithmeticOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean isArithmetic ()		{ return true; }
	
	public STO checkOperands(STO a, STO b)
	{
		Type aType = a.getType();
		Type bType = b.getType();
		if(!(aType.isNumeric()) || !(bType.isNumeric())){
			if(!(aType.isNumeric())){
				String ErrorStr = Formatter.toString(ErrorMsg.error1n_Expr, aType.getName(), this.getOperatorName());
				return new ErrorSTO(ErrorStr);
			}
			else{
				String ErrorStr = Formatter.toString(ErrorMsg.error1n_Expr, bType.getName(), this.getOperatorName());
				return new ErrorSTO(ErrorStr);
			}
		}
		else if(aType.isInt() && bType.isInt()){
			if (a.isConst() && b.isConst())
			{
				int aConst = ((ConstSTO)a).getIntValue();
				int bConst = ((ConstSTO)b).getIntValue();
				int resConst;
				
				if (this.isAdd())
					resConst = aConst + bConst;
				else if (this.isMinus())
					resConst = aConst - bConst;
				else if (this.isMultiply())
					resConst = aConst * bConst;
				else if (this.isDivide())
				{
					if (bConst == 0)
					{	
						String ErrorStr = ErrorMsg.error8_Arithmetic;
						return new ErrorSTO(ErrorStr);
					}
					else
						resConst = aConst / bConst;
				}
				else
					resConst = aConst % bConst;
				
				ConstSTO res = new ConstSTO(""+ resConst, new IntType("int", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new IntType("int",4));
		}
		else{
			if (a.isConst() && b.isConst())
			{
				float aConst = ((ConstSTO)a).getFloatValue();
				float bConst = ((ConstSTO)b).getFloatValue();
				float resConst;
				
				if (this.isAdd())
					resConst = aConst + bConst;
				else if (this.isMinus())
					resConst = aConst - bConst;
				else if (this.isMultiply())
					resConst = aConst * bConst;
				else if (this.isDivide())
				{
					if (bConst == 0)
					{	
						String ErrorStr = ErrorMsg.error8_Arithmetic;
						return new ErrorSTO(ErrorStr);
					}
					else
						resConst = aConst / bConst;
				}
				else
					resConst = aConst % bConst;
				
				ConstSTO res = new ConstSTO(""+ resConst, new FloatType("float", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new FloatType("float",4));
		}
	}
}

