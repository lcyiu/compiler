
abstract class Operator {
	
	private String name;
	
	public Operator(String strName){
		setName(strName);
	}
	
	private void setName(String n){
		name = n;
	}
	 
	public String getOperatorName(){
		return name;
	}
	
	
	//check 1.1
	public STO checkOperands(STO a, STO b) { 
			
		return new ErrorSTO("Should not called");
		
	}
	
	public STO checkOperand(STO a){ 
		
		return new ErrorSTO("Should not called");
		
	}
	
	public boolean isUnary()			{ return false;	}
	public boolean isBinary()			{ return false; }
	public boolean isNot()				{ return false;	}
	public boolean isInc()				{ return false; } 
	public boolean isDec()				{ return false;	}
	public boolean isArithmetic()		{ return false; } 
	public boolean isBoolean()			{ return false;	}
	public boolean isBitwise()			{ return false; } 
	public boolean isComparison()		{ return false;	}
	public boolean isAdd()				{ return false; } 
	public boolean isMinus()			{ return false;	}
	public boolean isMultiply()			{ return false; } 
	public boolean isDivide()			{ return false;	}
	public boolean isAnd()				{ return false; } 
	public boolean isOr()				{ return false;	}
	public boolean isBwAnd()			{ return false; } 
	public boolean isBwOr()				{ return false;	}
	public boolean isXor()				{ return false; } 
	public boolean isEqual()			{ return false; } 
	public boolean isNotEqual()			{ return false;	}
	public boolean isLessThan()			{ return false; } 
	public boolean isGreaterThan()		{ return false; } 
	public boolean isLessThanEqual()	{ return false;	}
	public boolean isMod()				{ return false; }
	public boolean isGreaterThanEqual()	{ return false; } 

	
	
	
	
}
