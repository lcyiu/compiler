
public class ArrayType extends CompositeType{
	public ArrayType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	
	public ArrayType(String strName, int size, Type bt, int NOE) {
		super(strName, size);
		setBaseType(bt);
		setNumOfElement(NOE);
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean isArray() 	{return true;}
	
	private void setBaseType(Type t)
	{
		baseType = t;
	}
	
	public Type getBaseType()
	{
		return baseType;
	}
	
	
	private void setNumOfElement(int i)
	{
		numOfElement = i;
	}
	
	public int getNumOfElement()
	{
		return numOfElement;
	}
	
	private Type baseType;
	private int numOfElement;
	
	public boolean	isAssignable (Type t) { return this.isEquivalent(t); }
	public boolean	isEquivalent (Type t) 
	{ 
		if (!t.isArray())
		{	
			return false;
		}
		
		
		ArrayType ta = (ArrayType)t;
		ArrayType tb = (ArrayType)this;
		int counter1 = 0;
		int counter2 = 0;
		int numelem1 = 0;
		int numelem2 = 0;
		//System.out.println(this.isSameType(t));
		//count the dimension of the array
		while(ta.getBaseType().isArray() && tb.getBaseType().isArray()){
			numelem1 = ta.getNumOfElement();
			numelem2 = tb.getNumOfElement();
		//	System.out.println("first:" + numelem1 + "   second:" + numelem2);
			ta = (ArrayType)ta.getBaseType();
			tb = (ArrayType)tb.getBaseType();
			counter1++;
			counter2++;
			if(numelem1 != numelem2){
				return false;
			}
		}
		numelem1 = ta.getNumOfElement();
		numelem2 = tb.getNumOfElement();
	//	System.out.println("first:" + numelem1 + "   second:" + numelem2);
		if(numelem1 != numelem2){
			return false;
		}
		while(tb.getBaseType().isArray()){
			numelem1 = ta.getNumOfElement();
			tb = (ArrayType)tb.getBaseType();
			counter2++;
		}

		
		
	//	System.out.println("t: " + counter1 + "this: " + counter2);
		
		if(counter2 != counter1)
		{
			return false;
		}
		
		if(ta.getNumOfElement() != tb.getNumOfElement())
		{
			return false;
		}
		
		if(!tb.getBaseType().isSameType(ta.getBaseType()))
		{
			return false;
		}
		return true;

	}
}
