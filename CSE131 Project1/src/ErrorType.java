
public class ErrorType extends Type
{
	public ErrorType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public boolean	isInt ()	{ return true; }
	public boolean	isBool()	{ return true; }
	public boolean 	isBasic()	{ return true; }
	public boolean 	isNumeric()	{ return true; }
	public boolean 	isFloat()	{ return true; }
	public boolean 	isVoid()	{ return true; }
	public boolean 	isCompsite()	{ return true; }
	public boolean 	isArray()	{ return true; }
	public boolean 	isStruct()	{ return true; }
	public boolean 	isPointerGroup()	{ return true; }
	public boolean 	isPointer()	{ return true; }
	public boolean 	isFunctionPointer()	{ return true; }
	public boolean 	isNullPointer()	{ return true; }
	
	
	public boolean	isAssignable (Type t) { return true; }
	public boolean	isEquivalent (Type t) { return true; }
	
}

