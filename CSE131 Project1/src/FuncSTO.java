import java.util.Vector;

//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class FuncSTO extends STO
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	FuncSTO (String strName)
	{
		super (strName);
		setReturnType (null);
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
	}
	
	public 
	FuncSTO (String strName, Type t)
	{
		super (strName, t);
		setReturnType (null);
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	isFunc () 
	{ 
		return true;
                // You may want to change the isModifiable and isAddressable                      
                // fields as necessary
	}


	//----------------------------------------------------------------
	// This is the return type of the function. This is different from 
	// the function's type (for function pointers).
	//----------------------------------------------------------------
	public void
	setReturnType (Type typ)
	{
		m_returnType = typ;
	}

	public Type
	getReturnType ()
	{
		return m_returnType;
	}
	
	public void
	setParam (Vector<STO> p)
	{
		params = p;
	}
	
	public Vector<STO>
	getParam ()
	{
		return params;
	}

	
	public int
	getNumParam ()
	{
		return params.size();
	}
	
	public void
	setreturnTypeIsRef (boolean b)
	{
		returnTypeIsRef = b;
	}

	public boolean
	getreturnTypeIsRef ()
	{
		return returnTypeIsRef;
	}
	
	public void
	addParamName (String param)
	{
		this.getType().setParamName(this.getType().getName()+ param);
	}
	
	


//----------------------------------------------------------------
//	Instance variables.
//----------------------------------------------------------------
	private Type 		m_returnType;
	private Vector<STO> params;
	private boolean 	returnTypeIsRef;
}
