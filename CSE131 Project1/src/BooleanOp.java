public class BooleanOp extends BinaryOp{
	public BooleanOp(String strName) {
		super(strName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isBoolean ()		{ return true; }
	
	public STO checkOperands(STO a, STO b)
	{
		Type aType = a.getType();
		Type bType = b.getType();
		
		if (aType.isBool() && bType.isBool())
		{
			if (a.isConst() && b.isConst())
			{
				boolean aConst = ((ConstSTO)a).getBoolValue();
				boolean bConst = ((ConstSTO)b).getBoolValue();
				String resConst;
				
				if (this.isAnd())
				{		
					resConst = (aConst && bConst)?"true":"false";
				}
				else
				{
					resConst = (aConst || bConst)?"true":"false";
				}
				
				ConstSTO res = new ConstSTO(resConst, new BoolType("bool", 4));
				return res;
			}
			else
				return new ExprSTO(a.getName() + this.getOperatorName() + b.getName(), new BoolType("bool",4));
		}
		else if(!aType.isBool()){
			String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, aType.getName(), this.getOperatorName(), "bool");
			return new ErrorSTO(ErrorStr);
		}
		else{
			String ErrorStr = Formatter.toString(ErrorMsg.error1w_Expr, bType.getName(), this.getOperatorName(), "bool");
			return new ErrorSTO(ErrorStr);
		}
			
	}
}

