import java.util.Vector;

public class StructType extends CompositeType{
	public StructType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public StructType(String strName, int size, Vector<STO> m) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setMember(m);
	}
	
	public StructType(String strName, int size, Vector<STO> m, String s) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setMember(m);
		setActualName(s);
	}
	
	public StructType(String strName, int size,  String s) {
		super(strName, size);
		// TODO Auto-generated constructor stub
		setActualName(s);
	}
	
	public boolean isStruct() 	{return true;}
	
	public Vector<STO> getMemeber() { return member; }
	
	private void setMember(Vector<STO> m) { member = m; }
	
	
	private Vector<STO> member;
	
	public void setSizeNParam(int s_size, Vector<STO> members) {
		// TODO Auto-generated method stub
		this.setSize(s_size);
		setMember(members);
		
	}
	
	public String getactualName() { return actualName; }
	public void setActualName( String s ) { actualName = s;}
	private String actualName;
	
	public boolean	isEquivalent (Type ta)
	{
		if(!ta.isStruct()){
			return false;
		}
		else
		{
			StructType st = (StructType)this;
			StructType st2 = (StructType)ta;
			if (!st.getactualName().equals(st2.getactualName()))
			{
				return false;
			}
			else
				return true;	
		}
	}
	
	public boolean	isAssignable (Type t) 
	{ 
		return isEquivalent(t);
	}
}

