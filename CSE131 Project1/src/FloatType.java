
public class FloatType extends NumericType{

	public FloatType(String strName, int size) {
		super(strName, size);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isFloat()	{ return true; }
	
	public boolean isAssignable (Type t)
	{
		return t.isNumeric();
	}
	
	public boolean isEquivalent (Type t)
	{
		return t.isFloat();
	}

}
