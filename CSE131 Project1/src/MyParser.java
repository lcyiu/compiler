
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java_cup.runtime.*;

import java.util.Vector;



class MyParser extends parser
{

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	MyParser (Lexer lexer, ErrorPrinter errors)
	{
		m_lexer = lexer;
		m_symtab = new SymbolTable ();
		m_errors = errors;
		m_nNumErrors = 0;
		
		m_codegen = new AssemblyCodeGenerator("rc.s");
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	Ok ()
	{
		return (m_nNumErrors == 0);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Symbol
	scan ()
	{
		Token		t = m_lexer.GetToken ();

		//	We'll save the last token read for error messages.
		//	Sometimes, the token is lost reading for the next
		//	token which can be null.
		m_strLastLexeme = t.GetLexeme ();

		switch (t.GetCode ())
		{
			case sym.T_ID:
			case sym.T_ID_U:
			case sym.T_STR_LITERAL:
			case sym.T_FLOAT_LITERAL:
			case sym.T_INT_LITERAL:
			case sym.T_CHAR_LITERAL:
				return (new Symbol (t.GetCode (), t.GetLexeme ()));
			default:
				return (new Symbol (t.GetCode ()));
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	syntax_error (Symbol s)
	{
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	report_fatal_error (Symbol s)
	{
		m_nNumErrors++;
		if (m_bSyntaxError)
		{
			m_nNumErrors++;

			//	It is possible that the error was detected
			//	at the end of a line - in which case, s will
			//	be null.  Instead, we saved the last token
			//	read in to give a more meaningful error 
			//	message.
			m_errors.print (Formatter.toString (ErrorMsg.syntax_error, m_strLastLexeme));
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	unrecovered_syntax_error (Symbol s)
	{
		report_fatal_error (s);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	DisableSyntaxError ()
	{
		m_bSyntaxError = false;
	}

	public void
	EnableSyntaxError ()
	{
		m_bSyntaxError = true;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String 
	GetFile ()
	{
		return (m_lexer.getEPFilename ());
	}

	public int
	GetLineNum ()
	{
		return (m_lexer.getLineNumber ());
	}

	public void
	SaveLineNum ()
	{
		m_nSavedLineNum = m_lexer.getLineNumber ();
	}

	public int
	GetSavedLineNum ()
	{
		return (m_nSavedLineNum);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoProgramStart()
	{
		// Opens the global scope.
		m_symtab.openScope ();
		
		m_codegen.DoProgramStart(GetFile());
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoProgramEnd()
	{
		m_symtab.closeScope ();
		m_codegen.dispose();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	/* Phase 1.1 */ /* Added Type arguments */
	void
	DoVarDecl (Vector<String> lstIDs, Type t, String isStatic)
	{
		Vector<STO> initList = getvarInitRunTime();
		Vector<STO> arrayList = getArray();
		boolean isGlobal = m_symtab.getFunc() == null && m_symtab.getLevel() == 1;
		boolean Static = isStatic.equals("static");
		
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = lstIDs.elementAt (i);
		
			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
				setError (true);
			}
			else if(t != null && !initList.get(i).isError() && !arrayList.get(i).isError()){		

				STO initElement = initList.get(i);
				STO arrayElement = arrayList.get(i);
				if (initElement.isEmpty()) //No Initialize
				{ 
					//Does not matter global or not
					//This is for Array
					if (!arrayElement.isEmpty())
					{
						if (!arrayElement.getType().isInt())
						{
							m_nNumErrors++;
							m_errors.print (Formatter.toString(ErrorMsg.error10i_Array, arrayElement.getType().getName()));
							setError (true);
						}
						else if (!arrayElement.isConst())
						{
							m_nNumErrors++;
							m_errors.print (ErrorMsg.error10c_Array);
							setError (true);
						}
						else if ( ((ConstSTO)arrayElement).getIntValue() <= 0)
						{
							m_nNumErrors++;
							m_errors.print (Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO)arrayElement).getIntValue()));
							setError (true);
						}
						else
						{
							ArrayType at = new ArrayType(t.getName()+"["+((ConstSTO)arrayElement).getIntValue()+"]"
									, t.getSize() * ((ConstSTO)arrayElement).getIntValue(), t, 
									((ConstSTO)arrayElement).getIntValue());
							VarSTO 		sto = new VarSTO (id, at);
							sto.setIsGlobal(isGlobal);
							
							m_symtab.insert (sto);
							
							//write assembly array
							if (sto.getIsGlobal() || Static)
							{
								m_codegen.DoGlobalDecl(sto, initElement, Static);
							}
							else
							{
								m_codegen.DoVarDecl(sto, initElement, false);
							}
						}
					}
					else
					{
						//this is just like :
						//int x;
						if (arrayElement.getName().equals("nothing"))
						{

							VarSTO 		sto = new VarSTO (id, t ,true, isGlobal);
							sto.setIsGlobal(isGlobal);
							m_symtab.insert (sto);
							
							if (sto.getIsGlobal() || Static)
							{
								m_codegen.DoGlobalDecl(sto, initElement, Static);
							}
							else
							{
								m_codegen.DoVarDecl(sto, initElement, false);
							}
							
						}
						else
						{
							//Pointer without initialization
							String star = arrayElement.getName();
							PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
							VarSTO 		sto = new VarSTO (id, pt);
							sto.setIsGlobal(isGlobal);
							m_symtab.insert (sto);
							
							if (sto.getIsGlobal() || Static)
							{
								m_codegen.DoGlobalDecl(sto, initElement, Static);
							}
							else
							{
								m_codegen.DoVarDecl(sto, initElement, false);
							}
						}

					}
				}
				else
				{
					if (!arrayElement.isEmpty())
					{
						//ARRAY WITH INITIALIZATION
						if (!arrayElement.getType().isInt())
						{
							m_nNumErrors++;
							m_errors.print (Formatter.toString(ErrorMsg.error10i_Array, arrayElement.getType().getName()));
							setError (true);
						}
						else if (!arrayElement.isConst())
						{
							m_nNumErrors++;
							m_errors.print (ErrorMsg.error10c_Array);
							setError (true);
						}
						else if ( ((ConstSTO)arrayElement).getIntValue() <= 0)
						{
							m_nNumErrors++;
							m_errors.print (Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO)arrayElement).getIntValue()));
							setError (true);
						}
						else
						{
							ArrayType at = new ArrayType(t.getName()+"["+((ConstSTO)arrayElement).getIntValue()+"]"
									, t.getSize() * ((ConstSTO)arrayElement).getIntValue(), t, 
									((ConstSTO)arrayElement).getIntValue());
							VarSTO 		sto = new VarSTO (id, at);
							m_symtab.insert (sto);
						}
					}
					else //else if Global static scope
					if ( (m_symtab.getFunc() == null && m_symtab.getLevel() == 1) || isStatic.equals("static"))
					{
						VarSTO sto;
						
						if (!arrayElement.getName().equals("nothing"))
						{
							//if it is a pointer type
							String star = arrayElement.getName();
							PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
							sto = new VarSTO (id, pt);
							sto.setIsGlobal(isGlobal);
							 
							if (!pt.isAssignable(initElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, initElement.getType().getName(), pt.getName()));
								setError (true);
							}
							else if (!initElement.isConst())
							{
								//if int x= not ConstSTO
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8a_CompileTime, id));
								setError (true);
							}
							else
							{
									//Pointer Type
									m_symtab.insert (sto);
									m_codegen.DoGlobalDecl(sto, initElement, Static);
							}
						}
						else
						{
							//if it is not a pointer

							
							sto = new VarSTO (id, t, true, isGlobal);
							
							if (!t.isAssignable(initElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, initElement.getType().getName(), t.getName()));
								setError (true);
							}
							else if (!initElement.isConst() && (!initElement.getType().isFunctionPointer() || !initElement.isFunc()))
							{
								//if int x= not ConstSTO
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8a_CompileTime, id));
								setError (true);
							}
							else
							{
									//Non Pointer 
									m_symtab.insert (sto);
									//Global 
									sto.setIsGlobal(isGlobal);
									m_codegen.DoGlobalDecl(sto, initElement, Static);
							}
						}

					}
					else	//local scope
					{
						VarSTO sto;
						
						if (!arrayElement.getName().equals("nothing"))
						{
							//if it is a pointer type
							String star = arrayElement.getName();
							PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
							sto = new VarSTO (id, pt);
							
							if (!pt.isAssignable(initElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, initElement.getType().getName(), pt.getName()));
								setError (true);
							}
							else
							{
									//Pointer
								//if int x = EXPRSTO		
								if (initElement.isConst())
								{
									m_codegen.DoVarDecl(sto, initElement, true);
								}
								else
								{
									m_codegen.DoVarDecl(sto, initElement, false);
								}
								m_symtab.insert (sto);
							}
						}
						else
						{
							//if it is not a pointer
							sto = new VarSTO (id, t);
							if (!t.isAssignable(initElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, initElement.getType().getName(), t.getName()));
								setError (true);
							}
							else
							{
								//if int x = EXPRSTO		
								if (initElement.isConst())
								{
									m_codegen.DoVarDecl(sto, initElement, true);
								}
								else
								{
									m_codegen.DoVarDecl(sto, initElement, false);
								}
									m_symtab.insert (sto);
							}
						}

					}
				}	
			}
			else{
				return;
			}
		}
	}
	
	void
	DoStrucVarDec (Type st, Vector<String> lstIDs)
	{
		Vector<STO> arrayList = getArray();
		
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = lstIDs.elementAt (i);
		
			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error13a_Struct, id));
			}
			else if(st != null  && !arrayList.get(i).isError()){		

				STO arrayElement = arrayList.get(i);
				
				Type t= changeType(st, arrayElement);
				

				//This is for Array
				if (!arrayElement.isEmpty())
				{
					if (st.isStruct() && st.getName().equals(getStructName()))
					{
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error13b_Struct, id));
					}
					else if (!arrayElement.getType().isInt())
					{
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error10i_Array, arrayElement.getType().getName()));
						
					}
					else if (!arrayElement.isConst())
					{
						m_nNumErrors++;
						m_errors.print (ErrorMsg.error10c_Array);
						
					}
					else if ( ((ConstSTO)arrayElement).getIntValue() < 0)
					{
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO)arrayElement).getIntValue()));
						
					}
					else
					{
						//ArrayType at = new ArrayType(t.getName()+"["+((ConstSTO)arrayElement).getIntValue()+"]"
						//		, t.getSize() * ((ConstSTO)arrayElement).getIntValue(), t, 
						//		((ConstSTO)arrayElement).getIntValue());
						VarSTO 		sto = new VarSTO (id, t);
						m_symtab.insert (sto);
						addStruct(sto);
					}
				}
				else
				{
					//this is just like :
					//int x;
					if (arrayElement.getName().equals("nothing"))
					{
						//decl same struct name in struct, size annot known
						if (st.isStruct() && st.getName().equals(getStructName()))
						{
							m_nNumErrors++;
							m_errors.print (Formatter.toString(ErrorMsg.error13b_Struct, id));
						}
						else
						{
							VarSTO 		sto = new VarSTO (id, t);
							m_symtab.insert (sto);
							addStruct(sto);
						}

					}
					else
					{
						//Pointer without initialization
						//String star = arrayElement.getName();
						//PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
						VarSTO 		sto = new VarSTO (id, t);
						m_symtab.insert (sto);
						addStruct(sto);
					}

				}

			}
		}
	}
	
	STO
	DoVarDeclParam (String id, Type t, String isRef)
	{
		STO sto;

		sto = new ErrorSTO(id);
		
		if (m_symtab.accessLocal (id) != null)
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
			setError (true);
		}
		else if(t != null){
			sto = new VarSTO(id, t);
			sto.setIsRef(isRef.equals("&"));
			
			m_symtab.insert (sto);
		}

		
		return sto;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoExternDecl (Vector<String> lstIDs, Type t)
	{
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = lstIDs.elementAt (i);

			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
				setError (true);
			}

			VarSTO 		sto = new VarSTO (id, t);
			sto.store("%g0", sto.getName(), sto.getName());
			m_symtab.insert (sto);
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoConstDecl (Vector<String> lstIDs, Type t, String isStatic)
	{
		Vector<STO> initList = getvarInitRunTime();
		
		boolean isGlobal = m_symtab.getFunc() == null && m_symtab.getLevel() == 1;
		boolean Static = isStatic.equals("static");
		
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = lstIDs.elementAt (i);

			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString (ErrorMsg.redeclared_id, id));
				setError (true);
			}
			else if(t != null && !initList.get(i).isError()){		

				STO initElement = initList.get(i);
				
				//if it is static or in global scope
				if (!initElement.isConst())
				{
					//if int x= not ConstSTO
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error8b_CompileTime, id));
					setError (true);
				}
				else if (!t.isAssignable(initElement.getType()))
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, initElement.getType().getName(), t.getName()));
					setError (true);
				}
				else
				{
					//if int x = ConstSTO
					ConstSTO 	sto = new ConstSTO (id, t, ((ConstSTO)initElement).getValue(), true);
					m_symtab.insert (sto);
					
					sto.setIsGlobal(isGlobal);
					
					if (sto.getIsGlobal() || Static)
					{
						m_codegen.DoGlobalDecl(sto, initElement, Static);
					}
					else
					{
						m_codegen.DoVarDecl(sto, initElement, true); 
					}
				}

			}
			else{
				return;
			}
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoTypedefDecl (Vector<String> lstIDs, Type t)
	{
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = lstIDs.elementAt (i);

			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
				setError (true);
			}
			
			if (t.getName().equals("")&&t.isInt() && t.getSize() == 0)
				continue;
			
			TypedefSTO 	sto = new TypedefSTO (id, t);
			m_symtab.insert (sto);
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoStructdefDecl (String id)
	{	
		if (m_symtab.accessLocal (id) != null)
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));

		}
		else
		{

			StructType st = new StructType(id, 0, id);
			TypedefSTO structSTO = new TypedefSTO(id, st);
			m_symtab.insert (structSTO);
		}
		

	}
	
	void DoFieldDec()
	{
		Vector<STO> members = getStruct();

		TypedefSTO struct = (TypedefSTO)m_symtab.access(getStructName());
		int s_size = 0;
		for (int i = 0; i< members.size(); i++)
		{
			s_size += members.get(i).getType().getSize();
		}
		for (int i = 0; i< members.size(); i++)
		{
			if (members.get(i).getType().isPointer())
			{
				/*
				if (((PointerType)members.get(i).getType()).getpointType().getName().equals(getStructName()) && 
						((PointerType)members.get(i).getType()).getpointType().getSize() == 0 &&
						((PointerType)members.get(i).getType()).getpointType().isStruct())
				{
					STO original= members.get(i);
					StructType oType = (StructType)((PointerType)original.getType()).getpointType();
					StructType newType = new StructType(oType.getName(), s_size, members);
					//TODO
					//PointerType newPointer 
					VarSTO newstructElement = new VarSTO (original.getName(), newType);
					members.set(i,newstructElement );
					//How to archieve *a.*a.*a.*a.*a .......
				}
				*/
			}

		}
		struct.getType().setSizeNParam(s_size, members);
		
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoFuncDecl_1 (String id, Type t, String ref, String extern)
	{
		if (m_symtab.accessLocal (id) != null)
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
			setError (true);
		}
		String functionName = "funcptr : " + t.getName() + " ";
		if (ref.equals("&"))
			functionName += ref + " ";

		
		FuncSTO sto = new FuncSTO (id, new FunctionPointerType(functionName, 0));
		sto.setReturnType(t);

		sto.setreturnTypeIsRef(ref.equals("&"));
		
		if (t.isArray() || t.isStruct())
		{
			sto.setreturnTypeIsRef(true);
		}
		
		m_symtab.insert (sto);

		m_symtab.openScope ();
		m_symtab.setFunc (sto);
		
		isFuncReturn= false;
		funcScopeLevel = m_symtab.getLevel();
		
		if (extern.equals(""))
			m_codegen.DoFuncStart(sto);
		else
			m_codegen.writeComment("Extern Function: " + sto.getName());
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoFuncDecl_2 (String extern)
	{
		FuncSTO curFunc = m_symtab.getFunc();
		if (curFunc != null)
		{
			if (extern.equals("") )
			{
				if (!isFuncReturn && !curFunc.getReturnType().isVoid())
				{
					m_nNumErrors++;
					m_errors.print (ErrorMsg.error6c_Return_missing);
					setError (true);
				}
			}
		}
		
		if (extern.equals(""))
			m_codegen.DoFuncFinish(curFunc);
		
		m_symtab.closeScope ();
		m_symtab.setFunc (null);
		
		

	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoFormalParams (Vector<STO> params, String extern)
	{
		//Assembly stack location
		String base = "%fp";
		int offset = 68;
		
		if (m_symtab.getFunc () == null)
		{
			m_nNumErrors++;
			m_errors.print ("internal: DoFormalParams says no proc!");
			setError (true);
		}
		else
		{
			Vector<STO> v_allParam = new Vector<STO>();
			String functionName = "(";
			if (params != null && params.size() > 0)
			{
				for (int i= 0; i< params.size(); i++)
				{

					
					STO p = params.get(i);
					
					//store assembly base and offset
					p.store(base, offset+"");
					offset +=4;
				
					if (!p.isError())
					{
						v_allParam.addElement(p);
						functionName+= p.getType().getName() + " ";
						functionName+= (p.getIsRef() || p.getType().isArray())?"&":"";
						functionName+= p.getName();
						if (i != params.size() - 1)
						{
							functionName+= ", ";
						}
					}
					
					if (params.get(i).getType().isArray() || params.get(i).getType().isStruct())
					{
						params.get(i).setIsRef(true);
					}
					//System.out.println("params " + p.getName() + " type = " + p.getType().getName() + " ref = " + p.getIsAddressable());			
				}
			}
			functionName+=")";
			m_symtab.getFunc ().addParamName(functionName);
			m_symtab.getFunc ().setParam(v_allParam);
			
			FunctionPointerType fpt = new FunctionPointerType(m_symtab.getFunc ().getType().getName(), 0, v_allParam, 
					m_symtab.getFunc ().getReturnType(), m_symtab.getFunc ().getIsRef());
			m_symtab.getFunc ().setType(fpt);
			
			//write input param in assembly
			if (extern.equals(""))
				m_codegen.writeParamInStack(m_symtab.getFunc ());
		}
		
		// insert parameters here
	}
	

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoBlockOpen ()
	{
		// Open a scope.
		m_symtab.openScope ();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoBlockClose()
	{
		m_symtab.closeScope ();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoAssignExpr (STO stoDes, STO stoTarget)
	{
		if(stoDes.isError()){
			setError (true);
			return stoDes;
		}
		if(stoTarget.isError()){
			setError (true);
			return stoTarget;
		}
		if (!stoDes.isModLValue())
		{
			// Good place to do the assign checks
			m_nNumErrors++;
		 	m_errors.print (ErrorMsg.error3a_Assign);	
		 	setError (true);
			return new ErrorSTO (stoDes.getName());
		}
		
		Type aType = stoDes.getType();
		Type bType = stoTarget.getType();
		
		
		if(!aType.isAssignable(bType)){
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error3b_Assign, bType.getName(), aType.getName()));
				setError (true);
				return (new ErrorSTO (stoTarget.getName ()));
		}	
		
		//Assign in Assembly
		m_codegen.DoAssign(stoDes, stoTarget);
		
		STO result = new ExprSTO(stoDes.getName(), stoDes.getType());
		result.store(stoDes.getBase(), stoDes.getOffset(), stoDes.getAsmName());
		result.setbArray(stoDes.getbArray());
		result.setIsArray(stoDes.getIsArray());
		result.setbStruct(stoDes.getbStruct());
		result.setIsStruct(stoDes.getIsStruct());
		result.setnIndex(stoDes.getnIndex());
		result.setIsRef(stoDes.getIsRef());
		result.setIsDeref(stoDes.getIsDeref());
		result.setbPointer(stoDes.getbPointer());
		result.setIsStatic(stoDes.getIsStatic());
		result.setIsGlobal(stoDes.getIsGlobal());
		
		return result;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoFuncCall (STO sto, Vector<STO> arg)
	{
		boolean errorFlag = false;
		
		if (!sto.isFunc() && !sto.getType().isFunctionPointer())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.not_function, sto.getName()));
			setError (true);
			return (new ErrorSTO (sto.getName ()));
		}
		else if (sto.isError())
		{
			setError (true);
			return sto;
		}
		
		if (sto.isFunc())
		{
			FuncSTO func = (FuncSTO) m_symtab.access(sto.getName());
	
			if (func != null)
			{
				//check num of arguments
				if (func.getNumParam() != arg.size())
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error5n_Call, arg.size(), func.getNumParam()));
					setError (true);
					return (new ErrorSTO (sto.getName ()));
				}
				//check pass by value assignable of type
				else 
				{
					Vector<STO> param = func.getParam();
					
					for (int i = 0; i< param.size(); i++)
					{
						STO argElement = arg.get(i);
						STO paramElement = param.get(i);
						
						//if argument is Error STO
						if (argElement.isError())
						{
							return argElement;
						}
						
						if (paramElement.getIsRef())
						{
							//check & equal type
							if (!paramElement.getType().isEquivalent(argElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5r_Call, argElement.getType().getName(), paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
							}
							else
							//check & mod L val
							if (!argElement.isModLValue() && !argElement.getType().isArray())
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5c_Call,  paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
							}
						}
						else
						{
							//check is assignable
							if (!paramElement.getType().isAssignable(argElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5a_Call, argElement.getType().getName(), paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
								
							}
						}
					}
				}
			}
			else
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, sto.getName()));
				setError (true);
				return (new ErrorSTO (sto.getName ()));
			}
			
			if (errorFlag)
			{
				setError (true);
				return (new ErrorSTO (sto.getName ()));	
			}
			else
			{
				STO result;
				
				//generate assembly code
				m_codegen.DoFuncCall(sto,  arg);
				
				if (func.getreturnTypeIsRef())
				{
					result = new VarSTO(sto.getName(), func.getReturnType());
					
				}
				else
				{
					result = new ExprSTO(sto.getName(), func.getReturnType());
				}
				
				m_codegen.storeReturnFromFunction(result, func.getreturnTypeIsRef());
				
				return result;
					
			}
		}
		else
		{	
			//THIS IS FUNCTION POINTER
			FunctionPointerType func= (FunctionPointerType) sto.getType();
			if (func != null)
			{	
				//check num of arguments
				if (func.getParams() == null && arg.size() ==0)
				{	
					STO result;
					
					//generate assembly code
					m_codegen.DoFuncCall(sto,  arg);
					
					if (func.getIsRef())
					{
						result = new VarSTO(sto.getName(), func.getReturnType());
						
					}
					else
					{
						result = new ExprSTO(sto.getName(), func.getReturnType());
					}
					
					m_codegen.storeReturnFromFunction(result, func.getIsRef());
					
					return result;
				}
				else
				if (func.getParams() == null && arg.size() !=0)
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error5n_Call, arg.size(), 0));
					setError (true);
					return (new ErrorSTO (sto.getName ()));
				}
				else
				if (func.getParams().size() != arg.size())
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error5n_Call, arg.size(), func.getParams().size()));
					setError (true);
					return (new ErrorSTO (sto.getName ()));
				}
				//check pass by value assignable of type
				else 
				{
					Vector<STO> param = func.getParams();
					
					for (int i = 0; i< param.size(); i++)
					{
						STO argElement = arg.get(i);
						STO paramElement = param.get(i);
						
						//if argument is Error STO
						if (argElement.isError())
						{
							return argElement;
						}
						
						if (paramElement.getIsRef())
						{
							//check & equal type
							if (!paramElement.getType().isEquivalent(argElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5r_Call, argElement.getType().getName(), paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
							}
							else
							//check & mod L val
							if (!argElement.isModLValue() && !argElement.getType().isArray())
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5c_Call,  paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
							}
						}
						else
						{
							//check is assignable
							if (!paramElement.getType().isAssignable(argElement.getType()))
							{
								m_nNumErrors++;
								m_errors.print (Formatter.toString(ErrorMsg.error5a_Call, argElement.getType().getName(), paramElement.getName(),
										paramElement.getType().getName()));
								setError (true);
								errorFlag= true;
								
							}
						}
					}
				}
			}
			else
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, sto.getName()));
				setError (true);
				return (new ErrorSTO (sto.getName ()));
			}
			
			if (errorFlag)
			{
				setError (true);
				return (new ErrorSTO (sto.getName ()));	
			}
			else
			{		
				
				STO result;
				
				//generate assembly code
				m_codegen.DoFuncCall(sto,  arg);
				
				if (func.getIsRef())
				{
					result = new VarSTO(sto.getName(), func.getReturnType());
					
				}
				else
				{
					result = new ExprSTO(sto.getName(), func.getReturnType());
				}
				
				m_codegen.storeReturnFromFunction(result, func.getIsRef());
				
				return result;
			}
		}
			
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator2_Dot (STO sto, String strID)
	{
		// Good place to do the struct checks
		
		if (sto.isError())
			return sto;
		
		Type resType = sto.getType();
		
		//TODO
		//Change resType to it original tpye if it is a typedef or array or pointer
		//MAYBE DONE :)
		
		if(!resType.isStruct())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error14t_StructExp, sto.getType().getName()));
			return (new ErrorSTO (sto.getName ()));
		}
		else
		{
			Vector<STO> members = ((StructType)resType).getMemeber();
			for (int i = 0; i < members.size(); i ++)
			{
				members.get(i).setIsStruct(true);
				members.get(i).setbStruct(sto);
				members.get(i).setIsRef(sto.getIsRef());
				members.get(i).setIsStatic(sto.getIsStatic());
				members.get(i).setIsGlobal(sto.getIsGlobal());
				
				if (members.get(i).getName().equals(strID))
				{
					if (members.get(i).getName().equals(strID) && members.get(i).getType().isPointer())
					{
						if (((PointerType)members.get(i).getType()).getpointType().isStruct())
						{
							StructType mst = (StructType) ((PointerType)members.get(i).getType()).getpointType();
							if (mst.getactualName().equals(((StructType)resType).getactualName()))
							{
								int size = sto.getType().getSize();
								((StructType)((PointerType)members.get(i).getType()).getpointType()).setSizeNParam(size, members);
								STO result = new VarSTO(members.get(i).getName(), members.get(i).getType());

								result.setbStruct(sto);
								result.setIsRef(sto.getIsRef());
								result.setIsStruct(true);
								result.setIsStatic(sto.getIsStatic());
								result.setIsGlobal(sto.getIsGlobal());
								
								return result;
							}
						}
						else
						{
							STO result = new VarSTO(members.get(i).getName(), members.get(i).getType());
							result.setIsRef(sto.getIsRef());
							result.setIsStruct(true);
							result.setbStruct(sto);
							result.setIsStatic(sto.getIsStatic());
							result.setIsGlobal(sto.getIsGlobal());
							return result;
						}	
					}
					else
					{
						STO result = new VarSTO(members.get(i).getName(), members.get(i).getType());
						result.setIsRef(sto.getIsRef());
						result.setIsStruct(true);
						result.setbStruct(sto);
						result.setIsStatic(sto.getIsStatic());
						result.setIsGlobal(sto.getIsGlobal());
						return result;
					}
				}
			}
			
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error14f_StructExp, strID, resType.getName()));
			return (new ErrorSTO (sto.getName ()));
			
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator2_Array (STO sto, STO nIndex)
	{
		// Good place to do the array checks
		if (sto.isError())
		{
			setError (true);
			return sto;
		}
		if (nIndex.isError())
		{
			setError (true);
			return nIndex;
		}
		
		if (!sto.getType().isArray() && !sto.getType().isPointer())
		{
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error11t_ArrExp, sto.getType().getName()));	
		 	setError (true);
			return new ErrorSTO (sto.getName());
		}
		else if (!nIndex.getType().isInt())
		{
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error11i_ArrExp, nIndex.getType().getName()));	
		 	setError (true);
			return new ErrorSTO (sto.getName());
		}
		else if (nIndex.isConst() && sto.getType().isArray())
		{
			//with bounds check : x[1]
			ArrayType at= (ArrayType)sto.getType();
			int arrMaxElement = at.getNumOfElement();
			int index = ((ConstSTO)nIndex).getIntValue();
			
			if (index < 0 || index >= arrMaxElement)
			{
				m_nNumErrors++;
			 	m_errors.print (Formatter.toString(ErrorMsg.error11b_ArrExp, index, arrMaxElement));
			 	setError (true);
				return new ErrorSTO (sto.getName());
			}
			else
			{
				STO result = new VarSTO(sto.getName(), ((ArrayType)sto.getType()).getBaseType(), true, nIndex, sto);
				result.setIsRef(sto.getIsRef());
				result.setIsStatic(sto.getIsStatic());
				result.setIsGlobal(sto.getIsGlobal());
				result.setbStruct(sto.getbStruct());
				result.setIsStruct(sto.getIsStruct());
				return result;
			}
		}
		else if (sto.getType().isArray() && !nIndex.isConst() )
		{
			STO result = new VarSTO(sto.getName(), ((ArrayType)sto.getType()).getBaseType(), true, nIndex, sto);
			result.setIsRef(sto.getIsRef());
			result.setIsStatic(sto.getIsStatic());
			result.setIsGlobal(sto.getIsGlobal());
			//no bound checking, for expr in index like x[a];
			m_codegen.DoRunTimeArrayChecking(sto, result);
			return result;
		}
		else
		{
			//Pointer Type
			if (sto.getType().isPointer())
			{
				PointerType pt= ((PointerType)sto.getType());
								
				if (pt.getStar() - 1 == 0)
				{
					STO res = DoCheckPointerDeref(sto);
					STO result = new VarSTO(sto.getName(), pt.getpointType(), true, nIndex, res);
					result.setIsRef(sto.getIsRef());
					result.setIsStatic(sto.getIsStatic());
					result.setIsGlobal(sto.getIsGlobal());
					result.setbStruct(sto.getbStruct());
					result.setIsStruct(sto.getIsStruct());
					return result;
				}
				else
				{
					PointerType newpt= new PointerType(pt.getName().substring(0, pt.getName().length()-1), 
							4, pt.getStar()-1, pt.getpointType());
					return new VarSTO(sto.getName(), newpt, true);
				}	
			}
		
		}
		
		return sto;

	}
	
	

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator3_ID (String strID)
	{
		STO		sto;

		if ((sto = m_symtab.access (strID)) == null)
		{
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, strID));	
		 	setError (true);
			sto = new ErrorSTO (strID);
		}
		return (sto);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoQualIdent (String strID)
	{
		STO		sto;

		if ((sto = m_symtab.access (strID)) == null)
		{
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, strID));	
		 	setError (true);
			return (new ErrorSTO (strID));
		}

		if (!sto.isTypedef())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.not_type, sto.getName ()));
			setError (true);
			return (new ErrorSTO (sto.getName ()));
		}

		return (sto);
	}
	
	/* Phase 1.0 */ /* Added */
	STO
	DoGlobalDesignator_ID (String strID)
	{
			STO		sto;

			if ((sto = m_symtab.accessGlobal (strID)) == null)
			{
				m_nNumErrors++;
			 	m_errors.print (Formatter.toString(ErrorMsg.error0g_Scope, strID));	
			 	setError (true);
				sto = new ErrorSTO (strID);
			}
			return (sto);
	} 
	
	/* Phase 1.1 */ /* In Progress */
	
	STO
	DoBinaryExpr(STO a, String o, STO b){
		
		
		if (a.isError())
		{
			setError (true);
			return a;
		}
		else if (b.isError())
		{
			setError (true);
			return b;
		}
		
		Operator op = DoOPDecl(o);
		STO result = op.checkOperands(a, b);
		
		if(result.isError()){
			m_nNumErrors++;
		 	m_errors.print (result.getName());	
		 	setError (true);
		}
		else
		{
			if (result.getType().isNumeric())
				m_codegen.DoArithmaticExpr(a, o, b, result);
			else if (result.getType().isBool())
				m_codegen.DoBooleanExpr(a, o, b, result);
		}
		
		return(result);
		
		
	}
	
	void 
	CheckAndOr(STO a, String o)
	{
		m_codegen.DoAndOrExpr(a, o);
	}
	
	STO
	DoUnaryExpr(STO a, String o, String prepost)
	{
		if (a.isError())
		{
			setError (true);
			return a;
		}
		
		Operator op = DoOPDecl(o);
		STO result = op.checkOperand(a);
		
		if(result.isError()){
			m_nNumErrors++;
		 	m_errors.print (result.getName());	
		 	setError (true);
		}
		
		//do assembly code gen
		m_codegen.DoIncDecOP(a, o, result, prepost);
		
		return(result);
	}
	
	STO
	DoUnarySignExpr(STO a, String u)
	{
		
		if (a.isError())
		{
			setError (true);
			return a;
		}
		else if (a.getType().isNumeric() && a.isConst() && u.equals("-"))
		{
			if (a.getIsAddressable())
			{
				STO result;
				if (a.getType().isInt())
					 result = new ConstSTO(-((ConstSTO)a).getIntValue()+"", a.getType(), -((ConstSTO)a).getValue());
				else
					 result = new ConstSTO(-((ConstSTO)a).getFloatValue()+"", a.getType(), -((ConstSTO)a).getValue());
				return result;
			}
			else
				return new ConstSTO("-"+a.getName(), a.getType(), -((ConstSTO)a).getValue());
		}
		else if (a.getType().isNumeric() && a.isConst() && u.equals("+"))
		{
			if (a.getIsAddressable())
			{
				STO result;
				if (a.getType().isInt())
					 result = new ConstSTO(((ConstSTO)a).getIntValue()+"", a.getType(), ((ConstSTO)a).getValue());
				else
					 result = new ConstSTO(((ConstSTO)a).getFloatValue()+"", a.getType(), ((ConstSTO)a).getValue());
				return result;
			}
			else
				return new ConstSTO(a.getName(), a.getType(), ((ConstSTO)a).getValue());
		}
		else if (!a.isConst())
		{
			String name = a.getName();
			if (u.equals("-"))
				name = "-"+name;
			ExprSTO result = new ExprSTO(name, a.getType());
			m_codegen.DoUnarySignExpr( a, result, u);
			return result ;
		}
		
		return(a);
	}
	
	/* Check 1.4 */
	void
	CheckIfWhile(STO a, String iw)
	{
		if(a.isError())
		{
			setError (true);
			return;
		}
		
		Type aType = a.getType();

		if(!(aType.isBool()) && !(aType.isInt()) ){
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error4_Test, aType.getName()));
			setError (true);

		}
		else
		{
			if (iw.equals("i"))
				m_codegen.DoIfStmt(a);
			if (iw.equals("w"))
				m_codegen.DoWhile(a);
		}
	}
	
	void
	CheckExit(STO a)
	{
		if(a.isError())
		{
			setError (true);
			return;
		}
		
		Type aType = a.getType();

		if(!(aType.isInt()) ){
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error7_Exit, aType.getName()));
			setError (true);

		}
		else
		{
			//code gen for assembly
			m_codegen.DoExit(a);
		}
	}
	
	Operator 
	DoOPDecl( String op)
	{
		
		if (op.equals("+"))
		{
			return new AddOp(op);
		}
		else if (op.equals("-"))
		{
			return new MinusOp(op);
		}
		else if (op.equals("*"))
		{
			return new MultiplyOp(op);
		}
		else if (op.equals("/"))
		{
			return new DivideOp(op);
		}
		else if (op.equals("%"))
		{
			return new ModOp(op);
		}
		else if (op.equals(">"))
		{
			return new GreaterThanOp(op);
		}
		else if (op.equals("<"))
		{
			return new LessThanOp(op);
		}
		else if (op.equals(">="))
		{
			return new GreaterThanEqualOp(op);
		}
		else if (op.equals("<="))
		{
			return new LessThanEqualOp(op);
		}
		else if (op.equals("=="))
		{
			return new EqualOp(op);
		}
		else if (op.equals("!="))
		{
			return new NotEqualOp(op);
		}
		else if (op.equals("||"))
		{
			return new OrOp(op);
		}
		else if (op.equals("&&"))
		{
			return new AndOp(op);
		}
		else if (op.equals("++"))
		{
			return new IncOp(op);
		}
		else if (op.equals("--"))
		{
			return new DecOp(op);
		}
		else if (op.equals("!"))
		{
			return new NotOp(op);
		}
		else if (op.equals("&"))
		{
			return new BwAndOp(op);
		}
		else if (op.equals("^"))
		{
			return new XorOp(op);
		}
		else 
		{
			return new BwOrOp(op);
		}


	}
	
	void 
	CheckReturnNoExpr()
	{
		FuncSTO func = m_symtab.getFunc();
		if (func != null)
		{
			if (!func.getReturnType().isVoid())
			{
				m_nNumErrors++;
				m_errors.print (ErrorMsg.error6a_Return_expr);
				setError (true);
			}
			else
				m_codegen.DoReturnNoExpr();
			
			if (m_symtab.getLevel() == funcScopeLevel)
			{
				isFuncReturn = true;
			}
			
			
		}
	}
	
	void 
	CheckReturnWithExpr(STO ret)
	{
		FuncSTO func = m_symtab.getFunc();
		if (func != null)
		{
			if (func.getreturnTypeIsRef())
			{
				if (!ret.isError())
				{
					if(!func.getReturnType().isEquivalent(ret.getType()))
					{
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error6b_Return_equiv, ret.getType().getName(), func.getReturnType().getName()));
						setError (true);
					}
					else if (!ret.isModLValue())
					{
						m_nNumErrors++;
						m_errors.print (ErrorMsg.error6b_Return_modlval);
						setError (true);
					}
				}
			}
			else
			{
				if (!ret.isError())
					if(!func.getReturnType().isAssignable(ret.getType()))
					{
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error6a_Return_type, ret.getType().getName(), func.getReturnType().getName()));
						setError (true);
					}
			}
			
			if (m_symtab.getLevel() == funcScopeLevel)
			{
				isFuncReturn = true;
			}
			
			m_codegen.DoReturnWithExpr(ret, func.getreturnTypeIsRef(), func.getReturnType().isFloat());
		}
		
	}
	
	void setNewVarInit() { varInitRunTime.clear(); }
	void addvarInit(STO s) { if (s !=null) varInitRunTime.addElement(s); }
	Vector<STO> getvarInitRunTime() { return varInitRunTime; }
	
	void setNewArray() { m_array.clear(); }
	void addArray(STO s) { if (s !=null) m_array.addElement(s); }
	Vector<STO> getArray() { return m_array; }
	
	void setNewStruct() { m_struct= null;
							m_struct = new Vector<STO>();}
	void addStruct(STO s) { if (s !=null) m_struct.addElement(s); }
	Vector<STO> getStruct() { return m_struct; }
	
	void setNewStructName() { m_structName = ""; }
	void setStructName(String s) {  m_structName = s; }
	String getStructName() { return  m_structName; }
	
	
	void incNumWhile()	{ m_numWhile ++; }
	void decNumWhile()	{ m_numWhile --; }
	int getNumWhile() 	{ return m_numWhile;}
	void checkBreak() 
	{
		if (getNumWhile()<= 0)
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error12_Break);
			setError (true);
		}
		else
		{
			m_codegen.DoBreak();
		}
	}
	void checkContinue() 
	{
		if (getNumWhile()<= 0)
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error12_Continue);
			setError (true);
		}
		else
		{
			m_codegen.DoContinue();
		}
	}
	
	Type
	findTypedefType (STO a)
	{
		
		if (!a.isError())
		{
			Type aType = a.getType();
			if (aType.isInt())
			{
				return new IntType(a.getName(),4);
			}
			else if (aType.isFloat())
			{
				return new FloatType(a.getName(),4);
			}
			else if (aType.isBool())
			{
				return new BoolType(a.getName(),4);
			}
			else if (aType.isArray())
			{
				ArrayType retT = new ArrayType(a.getName(), aType.getSize(), ((ArrayType)aType).getBaseType(), 
						((ArrayType)aType).getNumOfElement());	
				//Ohter type than Basic Tpye
				return retT;
			}
			else if (aType.isStruct())
			{
				return new StructType(a.getName(), aType.getSize(), ((StructType)aType).getMemeber(),((StructType)aType).getactualName());
			}
			else if (aType.isPointer())
			{
				return new PointerType(a.getName(), 4, ((PointerType)aType).getStar(),  ((PointerType)aType).getpointType());
			}
			else if (aType.isFunctionPointer())
			{
				FunctionPointerType afpt = (FunctionPointerType)aType;
				Type result = new FunctionPointerType(a.getName(), 4, afpt.getParams(), afpt.getReturnType(), afpt.getIsRef());
				result.setIsFuncPtr(true);
				return result;
			}
			else
			{
				return aType;
			}
		
			
		}
		else
		{
			if (getStructName().equals(a.getName()))
				return new StructType(a.getName(), 0, new Vector<STO>());
			else
				return new IntType("",0);
		}
	}
	
	Type
	changeType(Type t, STO s)
	{
		if (!s.isEmpty())
		{
			//is array
			if (!s.getType().isInt())
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error10i_Array, s.getType().getName()));
				setError (true);
				return new IntType("",0);
			}
			else if (!s.isConst())
			{
				m_nNumErrors++;
				m_errors.print (ErrorMsg.error10c_Array);
				setError (true);
				return new IntType("",0);
			}
			else if ( ((ConstSTO)s).getIntValue() < 0)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO)s).getIntValue()));
				setError (true);
				return new IntType("",0);
			}
			else
			{
				ArrayType at = new ArrayType(t.getName()+"["+((ConstSTO)s).getIntValue()+"]"
						, t.getSize() * ((ConstSTO)s).getIntValue(), t, 
						((ConstSTO)s).getIntValue());
				return at;
			}
		}
		else
		{
			if (s.isEmpty() && !s.isError() && !s.getName().equals("nothing"))
			{
				//Pointer without initialization
				String star = s.getName();
				PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
				return pt;
			}
			else
				return t;
		}
	}
	
	Type
	changePointerType(Type t, STO s)
	{
		if (s.isEmpty() && !s.isError() && !s.getName().equals("nothing"))
		{
			//Pointer without initialization
			String star = s.getName();
			PointerType pt = new PointerType (t.getName()+star, 4, star.length(), t);
			return pt;
		}
		else
		{
				return t;
		}
	}
	
	void setError (boolean b) { errorInOneStatement = b; }
	
	boolean getError() { return errorInOneStatement; }
	
	STO
	DoCheckPointerDeref (STO s)
	{
		if (s.isError())
			return s;
		else
		{
			Type sType = s.getType();
			
			if (!sType.isPointer() || sType.isNullPointer())
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error15_Receiver, sType.getName()));
				return (new ErrorSTO (s.getName ()));
			}
			else 
			{
					PointerType pt= ((PointerType)sType);
					STO result ;
					
					if (pt.getStar() - 1 == 0)
					{
						result = new VarSTO(s.getName(), pt.getpointType());

						m_codegen.DoStar(s, result);
						result.setIsDeref(true);
						result.setbPointer(s);
						result.setIsStatic(s.getIsStatic());
						result.setIsGlobal(s.getIsGlobal());
						result.setIsRef(s.getIsRef());
						return result;
					}
					else
					{
						PointerType newpt= new PointerType(pt.getName().substring(0, pt.getName().length()-1), 
								4, pt.getStar()-1, pt.getpointType());
						
						result = new VarSTO(s.getName(), newpt);

						m_codegen.DoStar(s, result);
						result.setIsDeref(true);
						result.setbPointer(s);
						result.setIsStatic(s.getIsStatic());
						result.setIsGlobal(s.getIsGlobal());
						result.setIsRef(s.getIsRef());
						
						return result;
					}	
			}
		}
	}
	
	STO
	DoDesignator2_Arrow(STO sto, String strID)
	{
		if (sto.isError())
			return sto;
		else
		{
			Type sType = sto.getType();
			
			//Fist Check if it is a pointer
			if (!sType.isPointer())
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error15_ReceiverArrow, sType.getName()));
				return (new ErrorSTO (sto.getName ()));
			}
			else
			{
				//then check if it is a pointer to a struc
				PointerType pt= (PointerType)sType;
				if (!pt.getpointType().isStruct() || pt.getStar() != 1)
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error15_ReceiverArrow, sType.getName()));
					return (new ErrorSTO (sto.getName ()));
				}
				else
				{
					//then check if the field is in the struct
					StructType st = (StructType)pt.getpointType();
					Vector<STO> members = st.getMemeber();
					if (members != null)
					{
						for (STO m : members)
						{
							
							if (m.getName().equals(strID) && m.getType().isPointer())
							{
								if (((PointerType)m.getType()).getpointType().isStruct())
								{
									StructType mst = (StructType) ((PointerType)m.getType()).getpointType();
									if (mst.getactualName().equals(st.getactualName()))
									{
										STO result = new VarSTO(m.getName(), sType);
										STO res = DoCheckPointerDeref(sto);
										result.setIsRef(sto.getIsRef());
										result.setIsStruct(true);
										result.setbStruct(res);
										result.setIsStatic(sto.getIsStatic());
										result.setIsGlobal(sto.getIsGlobal());
										return result;
									}
								}
							}
							if (m.getName().equals(strID))
							{
								STO res = DoCheckPointerDeref(sto);
								STO result = new VarSTO(m.getName(), m.getType());
								result.setIsRef(sto.getIsRef());
								result.setIsStruct(true);
								result.setbStruct(res);
								result.setIsStatic(sto.getIsStatic());
								result.setIsGlobal(sto.getIsGlobal());
								return result;
							}
							
						}
					}
					
					//if field not found, print error msg
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error14f_StructExp, strID, st.getName()));
					return (new ErrorSTO (sto.getName ()));
					
				}
				
			}
		}

	}
	
	STO
	DoAddressOf (STO sto)
	{
		if (sto.isError())
			return sto;
		
		

		if (!sto.getIsAddressable())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error21_AddressOf, sto.getType().getName()));
			return (new ErrorSTO (sto.getName ()));
		}
		else
		{
			PointerType pt = new PointerType (sto.getType().getName()+"*", 4, 1, sto.getType(), true);
			
			STO result = new ExprSTO(sto.getName(), pt);
			/*
			result.setbArray(sto.getbArray());
			result.setIsArray(sto.getIsArray());
			result.setbStruct(sto.getbStruct());
			result.setIsStruct(sto.getIsStruct());
			result.setnIndex(sto.getnIndex());
			result.setIsRef(sto.getIsRef());
			*/
			
			m_codegen.DoAddressOf(sto, result);
			
			return result;
		}
	}
	
	void
	DoNewDelete ( STO sto, String s)
	{
		if (sto.isError())
		{
			return;
		}
		else if (!sto.isModLValue())
		{
			m_nNumErrors++;
			if (s.equals("new"))
				m_errors.print (ErrorMsg.error16_New_var);
			else
				m_errors.print (ErrorMsg.error16_Delete_var);
		}
		else if (!sto.getType().isPointer())
		{
			m_nNumErrors++;
			if (s.equals("new"))
				m_errors.print (Formatter.toString(ErrorMsg.error16_New, sto.getType().getName()));
			else
				m_errors.print (Formatter.toString(ErrorMsg.error16_Delete, sto.getType().getName()));
		}
		else 
		{
			if (s.equals("new"))
			{
				m_codegen.DoNew(sto);
			}
			else
			{
				m_codegen.DoDelete(sto);
			}
		}
	}
	
	void
	DoAutoDecl (String strID, STO sto, boolean isConst, String isStatic)
	{
		if (m_symtab.accessLocal (strID) != null)
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, strID));
		}
		else
		{
			if (sto.isError())
			{
				return;
			}
			else
			{
				if (isConst)
				{
					if (!sto.isConst())
					{
						//if int x= not ConstSTO
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error8b_CompileTime, strID));
						setError (true);
					}
					else
					{
						boolean isGlobal = (m_symtab.getFunc() == null && m_symtab.getLevel() == 1);
						
						
						ConstSTO res = new ConstSTO(strID, sto.getType(), ((ConstSTO)sto).getValue(), true);
						res.setIsGlobal(isGlobal);
						if (isGlobal || isStatic.equals("static"))
						{
							m_codegen.DoGlobalDecl(res, sto, isStatic.equals("static"));
						}
						else
						{
							m_codegen.DoVarDecl(res, sto, isConst);
						}
						
						m_symtab.insert (res);
					}
				}
				else if ( (m_symtab.getFunc() == null && m_symtab.getLevel() == 1) || isStatic.equals("static"))
				{
					
					if (!sto.isConst())
					{
						//if int x= not ConstSTO
						m_nNumErrors++;
						m_errors.print (Formatter.toString(ErrorMsg.error8a_CompileTime, strID));
						setError (true);
					}
					else
					{
						VarSTO res = new VarSTO(strID, sto.getType());
						res.setIsGlobal(true);
						m_codegen.DoGlobalDecl(res, sto, isStatic.equals("static"));
						m_symtab.insert (res);
					}
				}
				else
				{
						VarSTO res = new VarSTO(strID, sto.getType());
						m_codegen.DoVarDecl(res, sto, isConst);
						m_symtab.insert (res);
				}
				
			}
		}
	}
	
	Type
	changeFuncPointerType(Type t, String isRef, Vector<STO> params)
	{
		String functionName = "funcptr : " + t.getName() + " ";
		if (isRef.equals("&"))
		{
			functionName += "& ";
		}
		functionName+= "(";
		if (params != null)
		{
			for (int i = 0; i < params.size(); i++)
			{
				STO p = params.get(i);
				if (!p.isError())
				{
					functionName+=p.getType().getName() + " ";
					if (p.getIsRef())
					{
						functionName += "&";
					}
					
					if (p.getType().isArray() || p.getType().isStruct())
					{
						p.setIsRef(true);
					}
					
					functionName += p.getName();
					if (i != params.size()-1)
					{
						functionName += ", ";
					}
				}
			}
			
		}
		functionName+= ")";
		
		Type result = new FunctionPointerType (functionName, 4, params, t, isRef.equals("&"));
		result.setIsFuncPtr(true);
		return result;
	}
	
	STO
	DoSizeOf(STO sto, Type t, String isSTOType)
	{
		if (isSTOType.equals("sto"))
		{
			if (sto.isError())
			{
				return sto;
			}
			else
			{
				if (!sto.getIsAddressable())
				{
					m_nNumErrors++;
					m_errors.print (ErrorMsg.error19_Sizeof);
					return (new ErrorSTO (sto.getName ()));
				}
				else
				{
					return new ConstSTO(""+sto.getType().getSize(), new IntType("int", 4), (double) sto.getType().getSize());
				}
			}
		}
		else if (isSTOType.equals("type")) 
		{
			return new ConstSTO(""+t.getSize(), new IntType("int", 4), (double) t.getSize());
		}
		else
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error19_Sizeof);
			return (new ErrorSTO ("noSizeOf"));
		}
	}
	
	STO
	DoTypeCast(STO sto, Type t)
	{
		if (sto.isError())
			return sto;
		else
		{
			//Check Type
			if (!t.isBasic() && !t.isPointer())
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error20_Cast, sto.getType().getName(), t.getName()));
				return (new ErrorSTO (sto.getName()));
			}
			else if (t.isPointer())
			{
				Type tBaseT = t;
				while (tBaseT.isPointer())
					tBaseT= ((PointerType)tBaseT).getpointType();
				
				if (!tBaseT.isBasic())
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error20_Cast, sto.getType().getName(), t.getName()));
					return (new ErrorSTO (sto.getName()));
				}
			}
			
			Type stoType = sto.getType();
			if (!stoType.isBasic() && !stoType.isPointer())
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error20_Cast, sto.getType().getName(), t.getName()));
				return (new ErrorSTO (sto.getName()));
			}
			else if (stoType.isPointer() && !stoType.isNullPointer())
			{
				while (stoType.isPointer())
				{
					stoType= ((PointerType)stoType).getpointType();
				}
				if (!stoType.isBasic())
				{
					m_nNumErrors++;
					m_errors.print (Formatter.toString(ErrorMsg.error20_Cast, sto.getType().getName(), t.getName()));
					return (new ErrorSTO (sto.getName()));
				}
			}
			
			double res = 0;
			//Finish Error Checking
			if (sto.isConst())
			{
				if (t.isNullPointer())
				{
					return new ConstSTO("nullptr", t, 0.0);
				}
				
				ConstSTO cSTO = (ConstSTO)sto;
				res = cSTO.getValue();
				Type baseT = t;
				Type typ  = sto.getType();
				
				while (baseT.isPointer())
				{
					baseT = ((PointerType)baseT).getpointType();
				}
				
				if (typ.isInt())
				{
					if (t.isBool())
					{
						
						if (res != 0)
							res = 1.0;
						return new ConstSTO((res!=0)?"true":"false", t, res);
					}
					if (t.isPointer())
						return new ConstSTO(res+"", t, res);
					if (t.isFloat())
					{
						return new ConstSTO(sto.getName()+".0", t, res);
					}
				}
				if (typ.isBool())
				{
					if (res != 0)
						res = 1.0;
					if (t.isInt() || t.isPointer())
						return new ConstSTO((res!=0)?"1":"0", t, res);
					if (t.isFloat())
						return new ConstSTO((res!=0)?"1.0":"0", t, res);
				}
				if (typ.isFloat())
				{
					if (t.isInt())
					{
						res = (double)((int)res);
						
						String name = (int)res+"";
						return new ConstSTO(name, t, res);
					}
					if (t.isBool())
					{
						if (res != 0)
							res = 1.0;
						return new ConstSTO((res!=0)?"true":"false", t, res);
					}
					if (t.isPointer())
					{
						res = (double)((int)res);
						
						String name = (int)res+"";
						return new ConstSTO(name, t, res);
					}
				}
				if (typ.isPointer())
				{
					if (t.isInt())
					{
						return new ConstSTO(res+"", t ,res);
					}
					if (t.isBool())
					{
						if (res != 0)
							res = 1.0;
						return new ConstSTO((res!=0)?"true":"false", t, res);
					}
					if (t.isFloat())
					{
						return new ConstSTO(sto.getName()+".0", t, res);
					}
				}

				/*					
				if (baseT.isInt() )
				{
					res = (double)((int)res);
					
					String name = (int)res+"";
					return new ConstSTO(name, t, res);
				}
				else if (baseT.isFloat())
				{
					return new ConstSTO(sto.getName(), t, res);
				}
				else
				{
					if (res != 0)
						res = 1.0;
					return new ConstSTO((res!=0)?"true":"false", t, res);
				}*/
				return sto;
			}
			else
			{
					STO result = new ExprSTO(sto.getName(), t);
					m_codegen.doTypecast(sto, t, result);
					//result.store(sto.getBase(), sto.getOffset(), sto.getAsmName());
					return result;
			}
		}
	}
	
	void DoCout (STO sto, boolean isEndl)
	{
		boolean isConst = sto.isConst();
		
		m_codegen.DoCout(sto, isConst, isEndl);
	
	}

	void DoEndIf (STO s)
	{
		m_codegen.DoEndIf(s);
	}
	
	void DoEndIfElse (STO s)
	{
		m_codegen.DoEndIfElse(s);
	}
	
	void DoCin (STO sto)
	{
		if (sto.isError())
			return;
		
		m_codegen.DoCin(sto);
	}
	
	void DoEndWhile (STO sto)
	{
		if (sto.isError())
			return;
		
		m_codegen.DoEndWhile(sto);
	}
	
	void DoStartWhile ()
	{	
		m_codegen.DoStartWhile();
	}
	
	void resetAndOr()
	{
		m_codegen.isAndOr = false;
	}
	
	void setAndOr(boolean b)
	{
		m_codegen.isAndOr = b;
	}
	
//----------------------------------------------------------------
//	Instance variables
//----------------------------------------------------------------
	private Lexer			m_lexer;
	private ErrorPrinter		m_errors;
	private int 			m_nNumErrors;
	private String			m_strLastLexeme;
	private boolean			m_bSyntaxError = true;
	private int			m_nSavedLineNum;
	
	private int 		funcScopeLevel;
	private boolean 	isFuncReturn = false;

	private SymbolTable		m_symtab;
	
	private Vector<STO>		varInitRunTime = new Vector<STO>();	
	private Vector<STO>		m_array = new Vector<STO>();
	private Vector<STO>		m_struct = new Vector<STO>();
	private String			m_structName = "";
	
	private int 		m_numWhile = 0;
	private boolean 	errorInOneStatement = false;
	private AssemblyCodeGenerator m_codegen;
	
	private int 		m_offset_func = 0;
	public static boolean		isDeclType = false;
	
	
	
}
