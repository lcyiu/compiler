/*
 * Generated Fri Jun 06 21:16:42 PDT 2014
 */

	
! |-------------------------------------------------------------------------
! |      Starting Program
! |-------------------------------------------------------------------------
	
	
	
! |-------------------------------------------------------------------------
! |      Default String Formatters
! |-------------------------------------------------------------------------
	
	.section	".rodata"
	_endl:	.asciz	"\n"
	_intFmt:	.asciz	"%d"
	_boolFmt:	.asciz	"%s"
	_boolT:	.asciz	"true"
	_boolF:	.asciz	"false"
	_strFmt:	.asciz	"%s"
	_nullpointerFmt:	.asciz	"Attempt to dereference NULL pointer.\n"
	_arrOutOfBoundFmt:	.asciz	"Index value of %d is outside legal range [0,%d).\n"
	.align	4
	_FloatINCDEC_1:	.single 0r	1.00
	
! Declare Global/Static: x
	.section	".bss"
	.align	4
	.global	x
! Array Type: size of int 4 * 10
x:	.skip	40
	
	
! |-------------------------------------------------------------------------
! |      Function: retX1
! |-------------------------------------------------------------------------
	
		
		.section	".text"
		.align	4
		.global	retX1
		
	retX1:
		set	SAVE.retX1, %g1
		save	%sp, %g1, %sp
		
	! Store %i0 - %i5 or %f0 - %f5 to stack space
		
	! Set 2 into %l1
		set	2, %l1
		
	! Store 2 into x
	! Store value of %l1 into x[0]
	! Get Offset of var x[0] and Store into %l4
	! Set 0 into %l0
		set	0, %l0
		
		sll	%l0, 2, %l0			! 0 * 4 -> scaled offset
		set	x, %l3
		add	%g0, %l3, %l3
		add	%l3, %l0, %l4
		st	%l1, [%l4]
		
	! Cout x
	! Set _intFmt into %o0
		set	_intFmt, %o0
		
	! Load x[0] into %o1
	! Get Offset of var x[0] and Store into %l4
	! Set 0 into %l0
		set	0, %l0
		
		sll	%l0, 2, %l0			! 0 * 4 -> scaled offset
		set	x, %l3
		add	%g0, %l3, %l3
		add	%l3, %l0, %l4
		ld	[%l4], %o1
		
		call	printf
		nop
		
	! Cout endl
	! Set _endl into %o0
		set	_endl, %o0
		
		call	printf
		nop
		
	! Return x
	! Load memory of x into %i0
	! Load x into %i0
		set	x, %l0
		add	%g0, %l0, %i0
		
		ret
		restore
		
		
	SAVE.retX1 = -(92 + 0) & -8
	
	
! |-------------------------------------------------------------------------
! |      Function: main
! |-------------------------------------------------------------------------
	
		
		.section	".text"
		.align	4
		.global	main
		
	main:
		set	SAVE.main, %g1
		save	%sp, %g1, %sp
		
	! Store %i0 - %i5 or %f0 - %f5 to stack space
		
	! STMT: Do Function Call : retX1
		call	retX1
		nop
		
		
	! Store return value to stack from function retX1
		set	-4, %l0
		add	%fp, %l0, %l0
		st	%o0, [%l0]
		
		
	! Declare Local: p1
	! base: %fp, offset: -8
	! Store retX1 into p1
	! Load retX1 into %l1
		set	-4, %l0
		add	%fp, %l0, %l0
		ld	[%l0], %l1
		
	! Store retX1 into p1
		set	-8, %l0
		add	%fp, %l0, %l0
		st	%l1, [%l0]
		
		
	! Cout x
	! Set _intFmt into %o0
		set	_intFmt, %o0
		
	! Load x[0] into %o1
	! Get Offset of var x[0] and Store into %l4
	! Set 0 into %l0
		set	0, %l0
		
		sll	%l0, 2, %l0			! 0 * 4 -> scaled offset
		set	x, %l3
		add	%g0, %l3, %l3
		add	%l3, %l0, %l4
		ld	[%l4], %o1
		
		call	printf
		nop
		
	! Cout endl
	! Set _endl into %o0
		set	_endl, %o0
		
		call	printf
		nop
		
	! EXPR: Star of p1
	! Load Variable address
	! Load p1 into %l1
		set	-8, %l0
		add	%fp, %l0, %l0
		ld	[%l0], %l1
		
	! Check for NULL pointer
		mov	%l1, %l5			! Moving value in %l1 into  %l5
		cmp	%g0, %l5
		bne	is_pointer_0
		nop
		
	! NULL Pointer : true
		set	_nullpointerFmt, %o0
		call	printf
		nop
		
		set	1, %o0
		call	exit
		nop
		
is_pointer_0:
	! Dereference the address
		ld	[%l1], %l1
	! Store address into tmp
		set	-12, %l0
		add	%fp, %l0, %l0
		st	%l1, [%l0]
		
		
	! Cout p1
	! Set _intFmt into %o0
		set	_intFmt, %o0
		
	! Load p1 into %o1
		set	-12, %l0
		add	%fp, %l0, %l0
		ld	[%l0], %o1
		
		call	printf
		nop
		
	! Cout  
	! Declare String " " at the end of function
	! Set main_str_0 into %o1
		set	main_str_0, %o1
		
	! Set _strFmt into %o0
		set	_strFmt, %o0
		
		call	printf
		nop
		
		
	! Cout x
	! Set _intFmt into %o0
		set	_intFmt, %o0
		
	! Load x[0] into %o1
	! Get Offset of var x[0] and Store into %l4
	! Set 0 into %l0
		set	0, %l0
		
		sll	%l0, 2, %l0			! 0 * 4 -> scaled offset
		set	x, %l3
		add	%g0, %l3, %l3
		add	%l3, %l0, %l4
		ld	[%l4], %o1
		
		call	printf
		nop
		
	! Cout endl
	! Set _endl into %o0
		set	_endl, %o0
		
		call	printf
		nop
		
		
		ret
		restore
		
	SAVE.main = -(92 + 12) & -8
	
! Declare Local Static variable in main
	.section	".rodata"
main_str_0:	.asciz	" "
